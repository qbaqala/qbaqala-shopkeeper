import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class OrdersService {
  apiRoute: any = {};

  constructor(private apiService: ApiService) {}

  bookOrder(recordData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/";
      this.apiRoute.data = recordData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getAllOrder() {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/get";
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getOrderById(orderId) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/orderById/";
      this.apiRoute.id = orderId;
      this.apiService
        .getData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getCustomerOrderById(orderId) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/userBookOrder/";
      this.apiRoute.id = orderId;
      this.apiService
        .getData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getBaqalaOrderById(baqalaId) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/baqalaBookOrder/";
      this.apiRoute.id = baqalaId;
      this.apiService
        .getData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getOrderByOrderValidity(orderData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/orderByExpTime";
      this.apiRoute.data = orderData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getPriceVal(orderData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "priceRange/byCountry/" + orderData;
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getOrderByStatus(orderData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/orderStatus/" + orderData;
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  updateOrder(id, dataObj) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/update/";
      this.apiRoute.id = id;
      this.apiRoute.data = dataObj;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  updateOrderStatus(id, dataObj) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/updateStatus/";
      this.apiRoute.id = id;
      this.apiRoute.data = dataObj;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  viewedOrders(id, dataObj) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "orderbook/updateArr/";
      this.apiRoute.id = id;
      this.apiRoute.data = dataObj;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  searchItem(itemName) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "items/findByName/" + itemName;
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  removeItem(itemId) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "items/delete/";
      this.apiRoute.id = itemId;
      this.apiService
        .deleteData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
