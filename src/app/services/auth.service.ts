import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  apiRoute: any = {};
  baqalaInfo: any = {};

  constructor(private apiService: ApiService) {}

  // Baqala

  baqalaSignin(loginData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqala/baqalaLogin";
      this.apiRoute.data = loginData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  baqalaForgotReq(reqData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqalaForgot/";
      this.apiRoute.data = reqData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  baqalaSignup(recordData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqalaRequest/";
      this.apiRoute.data = recordData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getCurrentBaqala(id) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqala/oneUser/";
      this.apiRoute.id = id;
      this.apiService
        .getData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  updateBaqala(id, dataObj) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqala/update/";
      this.apiRoute.id = id;
      this.apiRoute.data = dataObj;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  updateBaqalaPhone(id, phone) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqala/updatePhone/";
      this.apiRoute.id = id;
      this.apiRoute.data = phone;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  sendPushNotification(NotiData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "user/sendPushNoti";
      this.apiRoute.data = NotiData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  // sendPushNotification(NotiData) {
  //   return new Promise((resolve, reject) => {
  //     this.apiRoute.apiroute = "baqala/sendPushNoti/";
  //     this.apiRoute.data = NotiData;
  //     this.apiService
  //       .setData(this.apiRoute)
  //       .then((data: any) => {
  //         resolve(data);
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });
  // }

  // Customer

  sendCustomerNotification(NotiData) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "user/sendPushNoti/";
      this.apiRoute.data = NotiData;
      this.apiService
        .setData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getCurrentCustomer(id) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "user/oneUser/";
      this.apiRoute.id = id;
      this.apiService
        .getData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  updateCustomer(id, dataObj) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "user/update/";
      this.apiRoute.id = id;
      this.apiRoute.data = dataObj;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  updateCustomerPhone(id, phone) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "user/updatePhone/";
      this.apiRoute.id = id;
      this.apiRoute.data = phone;
      this.apiService
        .putData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getVersion() {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "baqala/getVersion";
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
