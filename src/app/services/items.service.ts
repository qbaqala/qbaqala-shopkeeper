import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class ItemsService {
  apiRoute: any = {};

  constructor(private apiService: ApiService) {}

  getAllItems() {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "items/getItem";
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  searchItem(itemName) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "items/findByName/" + itemName;
      this.apiService
        .get(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getItemById(itemId) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "items/userItems/";
      this.apiRoute.id = itemId;
      this.apiService
        .getData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  removeItem(itemId) {
    return new Promise((resolve, reject) => {
      this.apiRoute.apiroute = "items/delete/";
      this.apiRoute.id = itemId;
      this.apiService
        .deleteData(this.apiRoute)
        .then((data: any) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
