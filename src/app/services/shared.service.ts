import { Injectable } from "@angular/core";
import { AlertController } from "@ionic/angular";
import { BehaviorSubject, Observable } from "rxjs";
import { AuthService } from "./auth.service";
import { OrdersService } from "./orders.service";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  Photopath = "http://209.124.69.191:4000/images/";
  // Photopath = "http://localhost:4000/images/";

  info = {
    fullName: "",
    phone_number: "",
  };
  private baqalaDetailSubject: BehaviorSubject<any>;
  private baqalaDetails: Observable<any>;

  constructor(
    public alertCtrl: AlertController,
    private authService: AuthService,
    private orderService: OrdersService
  ) {
    if (localStorage.getItem("baqalaDetail")) {
      this.baqalaDetailSubject = new BehaviorSubject<any>(
        JSON.parse(localStorage.getItem("baqalaDetail"))
      );
    } else {
      this.baqalaDetailSubject = new BehaviorSubject<any>(this.info);
    }
    this.baqalaDetails = this.baqalaDetailSubject.asObservable();
  }

  getBaqalaInfo() {
    return this.baqalaDetailSubject.asObservable();
  }

  updateBaqalaDetail(data) {
    this.baqalaDetailSubject.next(data);
  }

  isLoggedIn() {
    if (localStorage.getItem("baqalaDetail")) return true;
    else return false;
  }

  sumItem(data) {
    if (toString.call(data) !== "[object Array]") return false;

    var total = 0;
    for (var i = 0; i < data.length; i++) {
      if (isNaN(data[i])) {
        continue;
      }

      total += Number(data[i]);
    }
    return total;
  }

  AddTime(hrs, mnt) {
    let expDate = new Date();
    expDate.setHours(expDate.getHours() + hrs);
    expDate.setMinutes(expDate.getMinutes() + mnt);
    let expireTime = expDate;
    return expireTime;
  }

  async simpleAlert(msg) {
    const alert = await this.alertCtrl.create({
      // cssClass: 'my-custom-class',
      header: "Message!!!",
      message: msg + ".",
      buttons: ["OK"],
    });
    await alert.present();
  }

  async titleAlert(title, msg) {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      header: title,
      message: msg,
      buttons: ["OK"],
    });
    await alert.present();
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      dist = dist * 1.609344; // distance in KM
      return dist.toFixed(1);
    }
  }

  sendNotification(notiObj) {
    let deviceIdArr = [];
    deviceIdArr.push(notiObj.deviceId);
    let NotiData = {
      deviceId: deviceIdArr,
      title: notiObj.notiTitle + "!!",
      massage: notiObj.notiDesc + ".",
    };
    this.authService
      .sendPushNotification(NotiData)
      .then((result) => {
        console.log(result, "notification sent success");
      })
      .catch((err) => {
        console.log(err, "unable to send notification");
      });
  }

  setPurgedStatus(orderId, order) {
    this.orderService
      .updateOrder(orderId, order)
      .then((res) => {
        console.log(res, "status setted to purged");
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
