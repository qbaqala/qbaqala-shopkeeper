import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { IonRouterOutlet, ModalController, Platform } from "@ionic/angular";
import { OrderInfoComponent } from "../order-info/order-info.component";
import { SelectDistanceComponent } from "../select-distance/select-distance.component";
import { SharedService } from "src/app/services/shared.service";
import { AuthService } from "src/app/services/auth.service";
import { OrdersService } from "src/app/services/orders.service";

declare var google;

@Component({
  selector: "app-shops",
  templateUrl: "./shops.component.html",
  styleUrls: ["./shops.component.scss"],
})
export class ShopsComponent implements OnInit {
  @ViewChild("map") mapElement: ElementRef;

  map: any;
  address: string;
  userDetail: any = {};
  userOrdersLocations: any = [];

  allOrders: any = [];
  allItems: any = [];
  orders: any = [];
  baqalaOrders: any = [];
  activeOrders: any = [];
  pastOrders: any = [];
  showing: any;
  filter: any;
  showMapList: any;
  navVal: any;
  btnActive: boolean = true;
  showPin: boolean = true;
  seeAllActive: boolean = false;
  seeAllPast: boolean = false;
  dateSort: boolean = true;
  itemSort: boolean = true;
  priceSort: boolean = true;
  distanceSort: boolean = true;
  distanceBtw: any;
  priceBtw: any;
  orderStatus: any = "placed";
  orderMarkers: any = {};
  buttonSubscription: any;
  country: any;
  priceRangeData: any = {};
  constructor(
    public modalCtrl: ModalController,
    private sharedService: SharedService,
    private authService: AuthService,
    private orderService: OrdersService,
    private platform: Platform,
    private routerOutlet: IonRouterOutlet
  ) {
    console.log(this.sharedService.isLoggedIn());
  }

  ngOnInit() {
    this.routerOutlet.swipeGesture = false;
    this.showing = "floatingOrder";
    this.filter = "date";
    this.showMapList = "map";
    this.navVal = "preparing";
  }

  backButtonEvent() {
    this.platform.ready().then(() => {
      this.buttonSubscription = this.platform.backButton.subscribeWithPriority(9999, () => {
        document.addEventListener(
          "backbutton",
          function (event) {
            event.preventDefault();
            event.stopPropagation();
            alert("back button is disabled");
          },
          false
        );
      });
    });
  }

  ionViewDidEnter() {
    // this.backButtonEvent();
    this.sharedService.getBaqalaInfo().subscribe((res) => {
      this.userDetail = res;
      if (this.userDetail._id) {
        this.orderStatus = "placed/" + this.userDetail.countryCode;
        this.getBaqalaOrder(this.navVal);
        if (this.userDetail.priceVal) console.log(this.userDetail, "main page Baqala");
        this.getAllOrder();
        this.timeLoop();
      }
      if(this.userDetail.countryCode == "+974"){
        this.country = "Qatar";
        this.getPriceValue()
      }
      if(this.userDetail.countryCode == "+91"){
        this.country = "India";
        this.getPriceValue()
      }
    });
  }

  getPriceValue() {
    this.orderService
    .getPriceVal(this.country)
    .then((res: any) => {
      this.priceRangeData = res[0];
      console.log(this.priceRangeData);
      this.filterByPrice();
    })
    .catch((err) => {
      console.log(err);
    });
  }

  timeLoop() {
    setInterval(() => {
      this.getLoopOrd(); 
      console.log("loop console")
    }, 30000);
  }

  getLoopOrd() {
    this.orderService
    .getOrderByStatus(this.orderStatus)
    .then((res: any) => {
      let loopArr = [];
      loopArr = res;
      if(loopArr.length != 0){
        if(parseInt(loopArr[loopArr.length - 1].orderCrtId) > parseInt(this.orders[0].orderCrtId)){
          this.getAllOrder();
        }
      }
    })
    .catch((err) => {
      console.log(err);
    });
  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.userDetail.latitude, this.userDetail.longitude);
    let options = {
      center: latLng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
    };

    setTimeout(() => {
      this.map = new google.maps.Map(this.mapElement.nativeElement, options);
      let mapMarkers: any = [];
      if (this.userOrdersLocations.length > 0) {
        mapMarkers = this.userOrdersLocations.sort(
          (a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
        );
        this.addMarkersToMap(mapMarkers);
      }
      this.addShopMarker();
    }, 200);
  }

  addShopMarker() {
    let position = new google.maps.LatLng(this.userDetail.latitude, this.userDetail.longitude);
    let mapMarkers = new google.maps.Marker({
      position: position,
      animation: google.maps.Animation.DROP,
      icon: {
        url: "../../../assets/imgs/shop.png",
        scaledSize: new google.maps.Size(45, 45),
      },
    });
    mapMarkers.setMap(this.map);
  }

  addMarkersToMap(markers) {
    for (let marker of markers) {
      let position = new google.maps.LatLng(marker.latitude, marker.longitude);
      this.orderMarkers = new google.maps.Marker({
        position: position,
        animation: google.maps.Animation.DROP,
        labelClass: "icon-label",

        label: {
          text: marker.totalPrice.toString(),
          color: "#fff",
          fontSize: "11px",
          marginBottom: "5px",
          fontFamily: "Quicksand-font",
        },
        icon: {
          url: "../../../assets/imgs/map-label.png",
          scaledSize: new google.maps.Size(54, 44),
          labelOrigin: new google.maps.Point(27, 16),
        },
        orderObj: marker,
        latitude: marker.latitude,
        longitude: marker.longitude,
      });

      this.orderMarkers.setMap(this.map);
      this.additionalWindowToMarker(this.orderMarkers);
    }
  }

  additionalWindowToMarker(marker) {
    marker.addListener("click", () => {
      console.log(marker.orderObj, "Marker");
      this.orderInfo(marker.orderObj);
    });
  }

  refreshOrder() {
    this.getAllOrder();
  }

  getBaqalaOrder(status) {
    this.orderService
      .getBaqalaOrderById(this.userDetail._id)
      .then((res: any) => {
        let ordersArr: any = [];
        let ordersSubStatus: any = [];
        let pastOrderStatus: any = [];
        this.activeOrders = [];
        this.pastOrders = [];
        res.forEach((order) => {
          if (order.status === "delivered") {
            pastOrderStatus.push(order);
            this.pastOrders = pastOrderStatus.sort(
              (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
            );
            this.pastOrders.forEach((element) => {
              element.totalItems = 0;
              element.items.forEach((item) => {
                element.totalItems = element.totalItems + item.itemQty;
              });
            });
          } else {
            ordersArr.push(order);
          }
        });
        ordersSubStatus = ordersArr.sort(
          (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        );
        if (status === "delivered") {
          this.activeOrders = this.pastOrders;
        } else {
          this.activeOrders = ordersSubStatus.filter((order) => order.subStatus == status);
          this.activeOrders.forEach((element) => {
            element.totalItems = 0;
            element.items.forEach((item) => {
              element.totalItems = element.totalItems + item.itemQty;
            });
          });
        }
        console.log(this.activeOrders, "activeOrders orders");
        console.log(this.baqalaOrders, "baqala orders");
      })
      .catch((err) => {
        console.log(err);
      });
  }

  seeMoreActive() {
    this.seeAllActive = !this.seeAllActive;
  }

  seeMorePast() {
    this.seeAllPast = !this.seeAllPast;
  }

  filterOrderLocation() {
    this.orderService
      .getOrderByStatus(this.orderStatus)
      .then((res: any) => {
        let ordersArr: any = [];
        ordersArr = res.sort(
          (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        );

        res.forEach((element) => {
          if (element.latitude && element.longitude) {
            this.userOrdersLocations.push(element);
          }
        });
        console.log(this.userOrdersLocations, "All Location Arr");
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getAllOrder() {
    const nonPurgedOrder: any = [];
    this.orderService
      .getOrderByStatus(this.orderStatus)
      .then((res: any) => {
        let currentDate = new Date().toISOString().split(".");
        res.forEach((orderArr) => {
          let orderExp = orderArr.orderVal.split(".");
          // console.log(currentDate[0], " : current ", orderExp[0], " : exp date");

          if (orderExp[0] < currentDate[0]) {
            console.log(orderArr, "Purged Orders");
            this.sharedService.setPurgedStatus(orderArr._id, {
              status: "purged",
              orderPurged: new Date(),
            });
          } else nonPurgedOrder.push(orderArr);
        });

        console.log(nonPurgedOrder, "Non Purged Orders");

        this.orders = nonPurgedOrder.sort(
          (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        );
        this.orders.forEach((element) => {
          element.isActive = false;
          if (element.latitude && element.longitude) {
            element.distance = this.sharedService.getDistanceFromLatLonInKm(
              this.userDetail.latitude,
              this.userDetail.longitude,
              element.latitude,
              element.longitude
            );
          } else {
            element.distance = 0;
          }
          element.totalProducyQty = 0;
          element.items.forEach((item) => {
            element.totalProducyQty = element.totalProducyQty + item.itemQty;
          });
        });
        console.log(this.orders, "all orders");
        this.filterByPrice();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  filterByPrice() {
    if (this.userDetail.priceVal == 1) {
      if(this.country == 'Qatar'){
        this.priceBtw = "QAR 0 - " + this.priceRangeData.initialThreshold;
      }
      if(this.country == 'India'){
        this.priceBtw = "INR 0 - " + this.priceRangeData.initialThreshold;
      }
      // this.priceBtw = "QAR 1 - 50";
      let ordersArr: any = [];
      ordersArr = this.orders.filter((order) => order.totalPrice >= 1 && order.totalPrice <= 50);
      this.fromPriceToDistance(ordersArr);
    } else if (this.userDetail.priceVal == 3) {
      if(this.country == 'Qatar'){
        this.priceBtw = "QAR "+ this.priceRangeData.initialThreshold +" - " + this.priceRangeData.finalThreshold;
      }
      if(this.country == 'India'){
        this.priceBtw = "INR "+ this.priceRangeData.initialThreshold +" - " + this.priceRangeData.finalThreshold;
      }
      // this.priceBtw = "QAR 51 - 150";
      let ordersArr: any = [];
      ordersArr = this.orders.filter((order) => order.totalPrice > 50 && order.totalPrice <= 150);
      this.fromPriceToDistance(ordersArr);
    } else if (this.userDetail.priceVal == 5) {
      if(this.country == 'Qatar'){
        this.priceBtw = "QAR"+ " > " + this.priceRangeData.finalThreshold;
      }
      if(this.country == 'India'){
        this.priceBtw = "INR"+ " > " + this.priceRangeData.finalThreshold;
      }
      // this.priceBtw = "QAR > 151";
      let ordersArr: any = [];
      ordersArr = this.orders.filter((order) => order.totalPrice > 150);
      this.fromPriceToDistance(ordersArr);
    } else if (this.userDetail.priceVal == 7) {
      this.priceBtw = "All Prices";
      this.fromPriceToDistance(this.orders);
    } else {
      this.priceBtw = "All Prices";
      this.fromPriceToDistance(this.orders);
    }
  }

  fromPriceToDistance(filteredPrice) {
    console.log(filteredPrice, "=== filtered prices ===");
    if (this.userDetail.distanceVal == 1) {
      this.distanceBtw = "0 - 1 km";
      let ordersArr: any = [];
      ordersArr = filteredPrice.filter((order) => order.distance <= 1);
      this.filterMarkers(ordersArr);
    } else if (this.userDetail.distanceVal == 3) {
      this.distanceBtw = "1 - 5 km";
      let ordersArr: any = [];
      ordersArr = filteredPrice.filter((order) => order.distance > 1 && order.distance <= 5);
      this.filterMarkers(ordersArr);
    } else if (this.userDetail.distanceVal == 5) {
      this.distanceBtw = ">5 km";
      let ordersArr: any = [];
      ordersArr = filteredPrice.filter((order) => order.distance > 5);
      this.filterMarkers(ordersArr);
    } else if (this.userDetail.distanceVal == 7) {
      this.distanceBtw = "All Distances";
      this.filterMarkers(filteredPrice);
    } else {
      this.distanceBtw = "0 - 1 km";
      let ordersArr: any = [];
      ordersArr = filteredPrice.filter((order) => order.distance <= 1);
      this.filterMarkers(ordersArr);
    }
  }

  filterMarkers(finalFilter) {
    console.log(finalFilter, "==== filtered Distances ===");
    this.allOrders = finalFilter;
    this.userOrdersLocations = [];
    this.allOrders.forEach((element) => {
      if (element.latitude && element.longitude) {
        this.userOrdersLocations.push(element);
      }
    });
    console.log(this.userOrdersLocations, "=== Final Filter location Arr ===");
    if (this.showMapList == "map") {
      setTimeout(() => {
        this.loadMap();
      }, 200);
    }
  }

  switchTo(val) {
    console.log(val);
    if (val == "myOrder") {
      this.showPin = false;
      this.getBaqalaOrder(this.navVal);
    } else {
      this.getAllOrder();
      this.showPin = true;
    }
    this.showing = val;
  }

  selectedNav(val) {
    this.navVal = val;
    if (val == "preparing") {
      this.getBaqalaOrder(this.navVal);
    } else if (val == "readyToDeliver") {
      this.getBaqalaOrder(this.navVal);
    } else if (val == "delivered") {
      this.getBaqalaOrder(this.navVal);
    }
  }

  filterBy(val) {
    this.filter = val;
    console.log(this.filter);

    if (val == "price") {
      if (this.priceSort == true) {
        this.priceSort = false;
        this.allOrders = this.allOrders.sort((a, b) => b.totalPrice - a.totalPrice);
      } else {
        this.priceSort = true;
        this.allOrders = this.allOrders.sort((a, b) => a.totalPrice - b.totalPrice);
      }
    } else if (val == "date") {
      if (this.dateSort == true) {
        this.dateSort = false;
        this.allOrders = this.allOrders.sort(
          (a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
        );
      } else {
        this.dateSort = true;
        this.allOrders = this.allOrders.sort(
          (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        );
      }
    } else if (val == "item") {
      if (this.itemSort == true) {
        this.itemSort = false;
        this.allOrders = this.allOrders.sort((a, b) => a.orderCrtId - b.orderCrtId);
      } else {
        this.itemSort = true;
        this.allOrders = this.allOrders.sort((a, b) => b.orderCrtId - a.orderCrtId);
      }
    } else if (val == "distance") {
      if (this.distanceSort == true) {
        this.distanceSort = false;
        this.allOrders = this.allOrders.sort((a, b) => a.distance - b.distance);
      } else {
        this.distanceSort = true;
        this.allOrders = this.allOrders.sort((a, b) => b.distance - a.distance);
      }
    }
  }

  selectTo(data) {
    console.log(data);
    this.getAllOrder();
    this.showMapList = data;
  }

  getUserDetail(data) {
    this.authService
      .getCurrentCustomer(data.userId)
      .then(async (res) => {
        console.log(res, "users detail");
        let orderInfos = {
          order: data,
          customer: res,
        };
        let ViewInfo = {
          baqalaUnqId: this.userDetail.baqalaUnqId,
          orderId: data._id,
        };
        this.orderService
          .viewedOrders(data._id, ViewInfo)
          .then((views) => {
            console.log(views, "Views setted");
          })
          .catch((err) => {
            console.log(err);
          });
        const modal = await this.modalCtrl.create({
          component: OrderInfoComponent,
          componentProps: { orderDetail: orderInfos },
        });
        modal.onDidDismiss().then((dataReturned) => {
          if (dataReturned !== null) {
            this.userDetail = JSON.parse(localStorage.getItem("baqalaDetail"));
            this.getAllOrder();
            this.getBaqalaOrder(this.navVal);
          }
        });
        return await modal.present();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  orderInfo(data) {
    this.getUserDetail(data);
  }

  async openDatail() {
    setTimeout(() => {
      this.btnActive = false;
    }, 500);
    const modal = await this.modalCtrl.create({
      component: SelectDistanceComponent,
      cssClass: "select-distance-modal",
      componentProps: { rangeData: this.userDetail },
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((dataReturned) => {
      this.btnActive = true;
      if (dataReturned !== null) {
        this.userDetail = dataReturned.data;
        this.getAllOrder();
      }
    });
    return await modal.present();
  }
}
