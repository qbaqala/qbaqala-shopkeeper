import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { SharedService } from "src/app/services/shared.service";

@Component({
  selector: "app-set-notification",
  templateUrl: "./set-notification.component.html",
  styleUrls: ["./set-notification.component.scss"],
})
export class SetNotificationComponent implements OnInit {
  notificationList: any = [
    {
      id: 1,
      name: "Notify when order is placed",
      isChecked: false,
    },
    {
      id: 2,
      name: "Notify when order is being viewed",
      isChecked: false,
    },
    {
      id: 3,
      name: "Notify when order is dispatched",
      isChecked: false,
    },
    {
      id: 4,
      name: "Notify when order is delivered",
      isChecked: false,
    },
    {
      id: 5,
      name: "Notify when order is cancelled",
      isChecked: false,
    },
    {
      id: 6,
      name: "Notify when order is purged",
      isChecked: false,
    },
  ];

  selectAll: boolean = false;
  checkDisable: boolean = false;
  notifications = {
    notification: false,
    orderPlaced: false,
    orderView: false,
    orderDispatch: false,
    orderDeliver: false,
    orderCancel: false,
    orderPurged: false,
  };
  baqalaDetail: any = {};
  constructor(
    private router: Router,
    private sharedService: SharedService,
    private authService: AuthService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    // this.sharedService.getBaqalaInfo().subscribe((res) => {
    //   this.baqalaDetail = res;
    //   console.log(this.baqalaDetail, "current Baqala");
    //   if (this.baqalaDetail._id) {
    //     this.setNotifications();
    //   } else {
    //     this.notifications.notification = true;
    //     this.notificationList.forEach((element) => {
    //       element.isChecked = true;
    //       this.selectAll = true;
    //       this.checkDisable = true;
    //     });
    //   }
    // });
  }

  setNotifications() {
    if (this.baqalaDetail.settings.notification == true) {
      this.notifications.notification = true;
      this.notificationList.forEach((element) => {
        element.isChecked = true;
        this.selectAll = true;
        this.checkDisable = true;
      });
    } else {
      this.notifications.notification = false;
      this.notificationList.forEach((element) => {
        element.isChecked = false;
        this.selectAll = false;
        this.checkDisable = false;
      });
    }
    this.notificationList.forEach((element) => {
      if (this.baqalaDetail.settings.orderPlaced == true && element.id == 1) {
        this.notifications.orderPlaced = true;
        element.isChecked = true;
      }
      if (this.baqalaDetail.settings.orderPlaced == false && element.id == 1) {
        this.notifications.orderPlaced = false;
        element.isChecked = false;
      }
      if (this.baqalaDetail.settings.orderView == true && element.id == 2) {
        this.notifications.orderView = true;
        element.isChecked = true;
      }
      if (this.baqalaDetail.settings.orderView == false && element.id == 2) {
        this.notifications.orderView = false;
        element.isChecked = false;
      }
      if (this.baqalaDetail.settings.orderDispatch == true && element.id == 3) {
        this.notifications.orderDispatch = true;
        element.isChecked = true;
      }
      if (this.baqalaDetail.settings.orderDispatch == false && element.id == 3) {
        this.notifications.orderDispatch = false;
        element.isChecked = false;
      }
      if (this.baqalaDetail.settings.orderDeliver == true && element.id == 4) {
        this.notifications.orderDeliver = true;
        element.isChecked = true;
      }
      if (this.baqalaDetail.settings.orderDeliver == false && element.id == 4) {
        this.notifications.orderDeliver = false;
        element.isChecked = false;
      }
      if (this.baqalaDetail.settings.orderCancel == true && element.id == 5) {
        this.notifications.orderCancel = true;
        element.isChecked = true;
      }
      if (this.baqalaDetail.settings.orderCancel == false && element.id == 5) {
        this.notifications.orderCancel = false;
        element.isChecked = false;
      }
      if (this.baqalaDetail.settings.orderPurged == true && element.id == 6) {
        this.notifications.orderPurged = true;
        element.isChecked = true;
      }
      if (this.baqalaDetail.settings.orderPurged == false && element.id == 6) {
        this.notifications.orderPurged = false;
        element.isChecked = false;
      }
    });
  }

  goBack() {
    this.router.navigate(["/shopkeeper"]);
  }

  setAllNotification($event) {
    let checked1 = $event.target.checked;
    this.selectAll = checked1;
    this.checkDisable = checked1;

    this.notificationList.forEach((item) => {
      item.isChecked = checked1;
      this.notifications.notification = checked1;
      this.notifications.orderPlaced = checked1;
      this.notifications.orderView = checked1;
      this.notifications.orderDispatch = checked1;
      this.notifications.orderDeliver = checked1;
      this.notifications.orderCancel = checked1;
      this.notifications.orderPurged = checked1;
    });
  }

  setNotification($event) {
    let checked1 = $event.target.checked;
    let checked2 = $event.target.value;
    if (checked2.isChecked == false) {
      checked2.isChecked = checked1;
    } else {
      checked2.isChecked = checked1;
    }
    if (checked2.id == 1) {
      this.notifications.orderPlaced = checked1;
    }
    if (checked2.id == 2) {
      this.notifications.orderView = checked1;
    }
    if (checked2.id == 3) {
      this.notifications.orderDispatch = checked1;
    }
    if (checked2.id == 4) {
      this.notifications.orderDeliver = checked1;
    }
    if (checked2.id == 5) {
      this.notifications.orderCancel = checked1;
    }
    if (checked2.id == 6) {
      this.notifications.orderPurged = checked1;
    }
    if (
      this.notifications.orderPlaced == true &&
      this.notifications.orderView == true &&
      this.notifications.orderCancel == true &&
      this.notifications.orderDeliver == true &&
      this.notifications.orderPurged == true &&
      this.notifications.orderDispatch == true
    ) {
      this.notifications.notification = true;
      this.selectAll = true;
      this.checkDisable = true;
    } else this.notifications.notification = false;
  }

  saveCahges() {
    // if (this.baqalaDetail._id) {
    //   this.baqalaDetail.settings.notification = this.notifications.notification;
    //   this.baqalaDetail.settings.orderPlaced = this.notifications.orderPlaced;
    //   this.baqalaDetail.settings.orderView = this.notifications.orderView;
    //   this.baqalaDetail.settings.orderDispatch = this.notifications.orderDispatch;
    //   this.baqalaDetail.settings.orderDeliver = this.notifications.orderDeliver;
    //   this.baqalaDetail.settings.orderCancel = this.notifications.orderCancel;
    //   this.baqalaDetail.settings.orderPurged = this.notifications.orderPurged;
    //   console.log(this.baqalaDetail, "saved detail");
    //   this.baqalaDetail.updatedAt = new Date();
    //   this.authService
    //     .updateBaqala(this.baqalaDetail._id, this.baqalaDetail)
    //     .then((res) => {
    //       console.log(res, "user updated");
    //       localStorage.setItem("baqalaDetail", JSON.stringify(res));
    //       this.sharedService.updateBaqalaDetail(res);
    //       this.sharedService.titleAlert("Success", "Notification settings changed successfully.");
    //     })
    //     .catch((err) => {
    //       console.log(err);
    //     });
    // }
  }
}
