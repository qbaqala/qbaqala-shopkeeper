import { Component, OnInit } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { OrdersService } from "src/app/services/orders.service";
import { SharedService } from "src/app/services/shared.service";

@Component({
  selector: "app-select-distance",
  templateUrl: "./select-distance.component.html",
  styleUrls: ["./select-distance.component.scss"],
})
export class SelectDistanceComponent implements OnInit {
  distanceObj: any = {};
  priceObj: any = {};
  userDetail: any = {};
  apiRoute: any = {};
  distanceVal: any;
  priceVal: any;
  distanceBtw: any = "0 - 1 km";
  priceBtw: any = "QAR 1 - 50";
  distanceCheck: boolean = false;
  priceCheck: boolean = false;
  priceRangeDisable: boolean = false;
  distanceRangeDisable: boolean = false;
  myVal;
  country: any;
  priceRangeData: any = {};

  constructor(
    public modalCtrl: ModalController,
    private authService: AuthService,
    public navParams: NavParams,
    private orderService: OrdersService,
  ) {
  }

  changeVal(change) {
    console.log(change.target.value, this.myVal, "change");
  }

  ngOnInit() {
    this.userDetail = this.navParams.get("rangeData");
    console.log(this.userDetail, "from baqala side");
    if(this.userDetail.countryCode == "+974"){
      this.country = "Qatar";
      this.getPriceValue()
    }
    if(this.userDetail.countryCode == "+91"){
      this.country = "India";
      this.getPriceValue()
    }

    if (this.userDetail.distanceVal == 1) {
      this.distanceVal = this.userDetail.distanceVal;
      this.distanceBtw = "0 - 1 km";
    } else if (this.userDetail.distanceVal == 3) {
      this.distanceVal = this.userDetail.distanceVal;
      this.distanceBtw = "1 - 5 km";
    } else if (this.userDetail.distanceVal == 5) {
      this.distanceVal = this.userDetail.distanceVal;
      this.distanceBtw = ">5 km";
    } else if (this.userDetail.distanceVal == 7) {
      this.distanceCheck = true;
      this.distanceBtw = "All Distances";
      this.distanceRangeDisable = true;
    } else {
      console.log("no distance val found");
    }

    // if (this.userDetail.priceVal == 1) {
    //   this.priceVal = this.userDetail.priceVal;
    //   this.priceBtw = "QAR 1 - 50";
    // } else if (this.userDetail.priceVal == 3) {
    //   this.priceVal = this.userDetail.priceVal;
    //   this.priceBtw = "QAR 51 - 150";
    // } else if (this.userDetail.priceVal == 5) {
    //   this.priceVal = this.userDetail.priceVal;
    //   this.priceBtw = "QAR > 151";
    // } else if (this.userDetail.priceVal == 7) {
    //   this.priceCheck = true;
    //   this.priceRangeDisable = true;
    //   this.priceBtw = "All Prices";
    // } else {
    //   console.log("no price val found");
    // }
  }

  getPriceValue() {
    this.orderService
    .getPriceVal(this.country)
    .then((res: any) => {
      this.priceRangeData = res[0];
      console.log(this.priceRangeData);

      if (this.userDetail.priceVal == 1) {
        this.priceVal = this.userDetail.priceVal;
        if(this.country == 'Qatar'){
          this.priceBtw = "QAR 0 - " + this.priceRangeData.initialThreshold;
        }
        if(this.country == 'India'){
          this.priceBtw = "INR 0 - " + this.priceRangeData.initialThreshold;
        }
        // this.priceBtw = "QAR 1 - 50";
      } else if (this.userDetail.priceVal == 3) {
        this.priceVal = this.userDetail.priceVal;
        if(this.country == 'Qatar'){
          this.priceBtw = "QAR "+ this.priceRangeData.initialThreshold +" - " + this.priceRangeData.finalThreshold;
        }
        if(this.country == 'India'){
          this.priceBtw = "INR "+ this.priceRangeData.initialThreshold +" - " + this.priceRangeData.finalThreshold;
        }
        // this.priceBtw = "QAR 51 - 150";
      } else if (this.userDetail.priceVal == 5) {
        this.priceVal = this.userDetail.priceVal;
        if(this.country == 'Qatar'){
          this.priceBtw = "QAR"+ " > " + this.priceRangeData.finalThreshold;
        }
        if(this.country == 'India'){
          this.priceBtw = "INR"+ " > " + this.priceRangeData.finalThreshold;
        }
        // this.priceBtw = "QAR > 151";
      } else if (this.userDetail.priceVal == 7) {
        this.priceCheck = true;
        this.priceRangeDisable = true;
        this.priceBtw = "All Prices";
      } else {
        console.log("no price val found");
      }
    })
    .catch((err) => {
      console.log(err);
    });
  }

  closeModal() {
    this.userDetail = JSON.parse(localStorage.getItem("baqalaDetail"));
    console.log(this.userDetail, "before close");
    this.modalCtrl.dismiss(this.userDetail);
  }

  allDistance($event) {
    let checked = $event.target.checked;
    this.distanceCheck = checked;
    if (this.distanceCheck == true) {
      this.authService.updateBaqala(this.userDetail._id, { distanceVal: 7 }).then(
        (data) => {
          console.log(data);
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.distanceRangeDisable = true;
          this.distanceBtw = "All Distances";
        },
        (err) => console.log(err)
      );
    } else {
      this.distanceVal = 1;
      this.authService.updateBaqala(this.userDetail._id, { distanceVal: 1 }).then(
        (data) => {
          console.log(data);
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.distanceBtw = "0 - 1 km";
          this.distanceRangeDisable = false;
        },
        (err) => console.log(err)
      );
    }
  }

  allPrice($event) {
    let checked = $event.target.checked;
    this.priceCheck = checked;
    if (this.priceCheck == true) {
      this.authService.updateBaqala(this.userDetail._id, { priceVal: 7 }).then(
        (data) => {
          console.log(data);
          this.priceRangeDisable = true;
          this.priceBtw = "All Prices";
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
        },
        (err) => console.log(err)
      );
    } else {
      this.priceVal = 1;
      this.authService.updateBaqala(this.userDetail._id, { priceVal: 1 }).then(
        (data) => {
          console.log(data);
          this.priceRange();
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.priceRangeDisable = false;
        },
        (err) => console.log(err)
      );
    }
  }

  distanceRange() {
    if (this.distanceVal == 1) {
      this.authService.updateBaqala(this.userDetail._id, { distanceVal: this.distanceVal }).then(
        (data) => {
          console.log(data);
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.distanceCheck = false;
          this.distanceBtw = "0 - 1 km";
        },
        (err) => console.log(err)
      );
    } else if (this.distanceVal == 3) {
      this.authService.updateBaqala(this.userDetail._id, { distanceVal: this.distanceVal }).then(
        (data) => {
          console.log(data);
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.distanceCheck = false;
          this.distanceBtw = "1 - 5 km";
        },
        (err) => console.log(err)
      );
    } else if (this.distanceVal == 5) {
      this.authService.updateBaqala(this.userDetail._id, { distanceVal: this.distanceVal }).then(
        (data) => {
          console.log(data);
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.distanceBtw = ">5 km";
          this.distanceCheck = false;
        },
        (err) => console.log(err)
      );
    }
  }

  priceRange() {
    if (this.priceVal == 1) {
      this.authService.updateBaqala(this.userDetail._id, { priceVal: this.priceVal }).then(
        (data) => {
          console.log(data);
          if(this.country == 'Qatar'){
            this.priceBtw = "QAR 0 - " + this.priceRangeData.initialThreshold;
          }
          if(this.country == 'India'){
            this.priceBtw = "INR 0 - " + this.priceRangeData.initialThreshold;
          }
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.priceCheck = false;
        },
        (err) => console.log(err)
      );
    } else if (this.priceVal == 3) {
      this.authService.updateBaqala(this.userDetail._id, { priceVal: this.priceVal }).then(
        (data) => {
          console.log(data);
          if(this.country == 'Qatar'){
            this.priceBtw = "QAR "+ this.priceRangeData.initialThreshold +" - " + this.priceRangeData.finalThreshold;
          }
          if(this.country == 'India'){
            this.priceBtw = "INR "+ this.priceRangeData.initialThreshold +" - " + this.priceRangeData.finalThreshold;
          }
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.priceCheck = false;
        },
        (err) => console.log(err)
      );
    } else if (this.priceVal == 5) {
      this.authService.updateBaqala(this.userDetail._id, { priceVal: this.priceVal }).then(
        (data) => {
          console.log(data);
          if(this.country == 'Qatar'){
            this.priceBtw = "QAR"+ " > " + this.priceRangeData.finalThreshold;
          }
          if(this.country == 'India'){
            this.priceBtw = "INR"+ " > " + this.priceRangeData.finalThreshold;
          }
          localStorage.setItem("baqalaDetail", JSON.stringify(data));
          this.priceCheck = false;
        },
        (err) => console.log(err)
      );
    }
  }
}
