import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { Router } from "@angular/router";
import { SharedService } from "src/app/services/shared.service";
import { AuthService } from "src/app/services/auth.service";
import { OrdersService } from "src/app/services/orders.service";

@Component({
  selector: "app-default-view-range",
  templateUrl: "./default-view-range.component.html",
  styleUrls: ["./default-view-range.component.scss"],
})
export class DefaultViewRangeComponent implements OnInit {
  distances: any = [
    {
      val: "0 - 1 kms",
      isActive: false,
      range: 1,
    },
    {
      val: "1 - 5 kms",
      isActive: false,
      range: 3,
    },
    {
      val: ">5 kms",
      isActive: false,
      range: 5,
    },
    {
      val: "All Distances",
      isActive: false,
      range: 7,
    },
  ];
  prices: any = [
    {
      val: "QAR 1 - 50",
      isActive: false,
      range: 1,
    },
    {
      val: "QAR 51 - 150",
      isActive: false,
      range: 3,
    },
    {
      val: "QAR > 151",
      isActive: false,
      range: 5,
    },
    {
      val: "All Prices",
      isActive: false,
      range: 7,
    },
  ];
  pricesIndia: any = [
    {
      val: "INR 0 - 500",
      isActive: false,
      range: 1,
    },
    {
      val: "INR 500 - 1000",
      isActive: false,
      range: 3,
    },
    {
      val: "INR > 1000",
      isActive: false,
      range: 5,
    },
    {
      val: "All Prices",
      isActive: false,
      range: 7,
    },
  ];

  distanceData: any = {};
  priceData: any = {};
  apiRoute: any = {};
  userDetail: any = {};
  changedObj: any = {};
  country: any;
  priceRangeData: any = {};
  constructor(
    public modalCtrl: ModalController,
    private router: Router,
    private authService: AuthService,
    private orderService: OrdersService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    if (localStorage.getItem("baqalaDetail")) {
      this.userDetail = JSON.parse(localStorage.getItem("baqalaDetail"));
      console.log(this.userDetail);
      if(this.userDetail.countryCode == "+974"){
        this.country = "Qatar";
        this.getPriceValue();

        this.prices.forEach((element) => {
          if (element.range == this.userDetail.priceVal) {
            this.priceData.range = this.userDetail.priceVal;
            element.isActive = true;
          }
        });
      }
      if(this.userDetail.countryCode == "+91"){
        this.country = "India";
        this.getPriceValue();

        this.pricesIndia.forEach((element) => {
          if (element.range == this.userDetail.priceVal) {
            this.priceData.range = this.userDetail.priceVal;
            element.isActive = true;
          }
        });
      }

      this.distances.forEach((element) => {
        if (element.range == this.userDetail.distanceVal) {
          this.distanceData.range = this.userDetail.distanceVal;
          element.isActive = true;
        }
      });

      // this.prices.forEach((element) => {
      //   if (element.range == this.userDetail.priceVal) {
      //     this.priceData.range = this.userDetail.priceVal;
      //     element.isActive = true;
      //   }
      // });
    } else {
      this.distances[0].isActive = true;
      this.prices[0].isActive = true;
    }
  }

  getPriceValue() {
    this.orderService
    .getPriceVal(this.country)
    .then((res: any) => {
      this.priceRangeData = res[0];
      console.log(this.priceRangeData);
    })
    .catch((err) => {
      console.log(err);
    });
  }

  goBack() {
    this.router.navigate(["/shopkeeper"]);
  }

  selectedDistance(data) {
    this.distanceData = data;

    this.distances.forEach((element) => {
      if (element.val == data.val) element.isActive = true;
      else element.isActive = false;
    });
  }

  selectedPrices(data) {
    this.priceData = data;

    if(this.country == 'Qatar'){
      this.prices.forEach((element) => {
        if (element.val == data.val) element.isActive = true;
        else element.isActive = false;
      });
    }
    if(this.country == 'India'){
      this.pricesIndia.forEach((element) => {
        if (element.val == data.val) element.isActive = true;
        else element.isActive = false;
      });
    }
  }

  saveChanges() {
    console.log(this.distanceData.range);
    console.log(this.priceData.range);
    if (this.distanceData.range) {
      this.changedObj.distanceVal = this.distanceData.range;
      console.log(this.changedObj);
    }
    if (this.priceData.range) {
      this.changedObj.priceVal = this.priceData.range;
      console.log(this.changedObj);
    }

    if (this.changedObj.priceVal || this.changedObj.distanceVal) {
      this.authService.updateBaqala(this.userDetail._id, this.changedObj).then(
        (data) => {
          console.log(data);
          this.userDetail = data;
          localStorage.setItem("baqalaDetail", JSON.stringify(this.userDetail));
          this.sharedService.updateBaqalaDetail(this.userDetail);
          this.router.navigate(["/shopkeeper"]);
        },
        (err) => {
          console.log(err);
          if (err.error) {
            this.sharedService.simpleAlert("Somthing went wrong");
          }
        }
      );
    } else {
      this.sharedService.simpleAlert("select any filter");
    }
  }
}
