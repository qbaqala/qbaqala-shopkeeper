import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { OrdersService } from "src/app/services/orders.service";
import { SharedService } from "src/app/services/shared.service";

@Component({
  selector: "app-set-delivery-time",
  templateUrl: "./set-delivery-time.component.html",
  styleUrls: ["./set-delivery-time.component.scss"],
})
export class SetDeliveryTimeComponent implements OnInit {
  showLabel: boolean = true;
  time1: any = [
    {
      val: "30 mins",
      isActive: false,
      distanceVal: 1,
      range: 1,
    },
    {
      val: "45 mins",
      isActive: false,
      distanceVal: 1,
      range: 3,
    },
    {
      val: "1 hour",
      isActive: false,
      distanceVal: 1,
      range: 5,
    },
    {
      val: "2 hour",
      isActive: false,
      distanceVal: 1,
      range: 7,
    },
  ];
  time2: any = [
    {
      val: "30 mins",
      isActive: false,
      distanceVal: 3,
      range: 1,
    },
    {
      val: "45 mins",
      isActive: false,
      distanceVal: 3,
      range: 3,
    },
    {
      val: "1 hour",
      isActive: false,
      distanceVal: 3,
      range: 5,
    },
    {
      val: "2 hour",
      isActive: false,
      distanceVal: 3,
      range: 7,
    },
  ];
  time3: any = [
    {
      val: "30 mins",
      isActive: false,
      distanceVal: 5,
      range: 1,
    },
    {
      val: "45 mins",
      isActive: false,
      distanceVal: 5,
      range: 3,
    },
    {
      val: "1 hour",
      isActive: false,
      distanceVal: 5,
      range: 5,
    },
    {
      val: "2 hour",
      isActive: false,
      distanceVal: 5,
      range: 7,
    },
  ];
  prices: any;

  distance1Data: any = {};
  distanceData: any = {};
  distance2Data: any = {};
  distance3Data: any = {};
  apiRoute: any = {};
  userDetail: any = {};
  changedObj: any = {};

  constructor(
    private router: Router,
    private sharedService: SharedService,
    private orderService: OrdersService
  ) {}

  ngOnInit() {}

  goBack() {
    this.router.navigate(["/shopkeeper"]);
  }

  distance1(data) {
    this.distanceData = data;

    this.time1.forEach((element) => {
      if (element.val == data.val) element.isActive = true;
      else element.isActive = false;
    });
    this.time2.forEach((element) => {
      element.isActive = false;
    });
    this.time3.forEach((element) => {
      element.isActive = false;
    });
  }

  distance2(data) {
    this.distanceData = data;
    this.time2.forEach((element) => {
      if (element.val == data.val) element.isActive = true;
      else element.isActive = false;
    });
    this.time1.forEach((element) => {
      element.isActive = false;
    });
    this.time3.forEach((element) => {
      element.isActive = false;
    });
  }

  distance3(data) {
    this.distanceData = data;
    this.time3.forEach((element) => {
      if (element.val == data.val) element.isActive = true;
      else element.isActive = false;
    });
    this.time2.forEach((element) => {
      element.isActive = false;
    });
    this.time1.forEach((element) => {
      element.isActive = false;
    });
  }

  ionViewWillEnter() {
    this.showLabel = true;
  }

  selectTime() {
    this.showLabel = false;
  }

  splitMethod(data) {
    let splitVal = data.split(" ");
    let splited = parseInt(splitVal[0]);
    return splited;
  }

  getOrderDeliveryTime(data) {
    this.orderService
      .getOrderByOrderValidity(data)
      .then((res) => {
        console.log(res, "fetched order");
        this.sharedService.titleAlert("Success", "Order fetched successfully.");
        this.router.navigate(["/shopkeeper"]);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  timeChangesApply() {
    let split = this.distanceData.val.split(" ");
    let hrs, mins;

    // if (this.distanceData.range && this.distanceData.distanceVal == 1) {

    // }
    // if (this.distanceData.range && this.distanceData.distanceVal == 3) {
    //   // this.changedObj.timeVal = this.distanceData.range;
    //   // console.log(this.distanceData);
    // }
    // if (this.distanceData.range && this.distanceData.distanceVal == 5) {
    //   // this.changedObj.timeVal = this.distanceData.range;
    //   // console.log(this.distanceData);
    // }

    // console.log(this.distanceData);
    // if (this.distance2Data.range) {
    //   this.changedObj.timeVal = this.distance2Data.range;
    //   console.log(this.distance2Data);
    // }
    // if (this.distance3Data.range) {
    //   this.changedObj.timeVal = this.distance3Data.range;
    //   console.log(this.distance2Data);
    // }
    // console.log(this.changedObj);
    // if (this.changedObj.val || this.changedObj.val) {
    // }
    // console.log(this.distance1Data, this.distance2Data, this.distance3Data);
    let currentTime = new Date();
    let times = this.splitMethod(this.distanceData.val);
    if (split[1] == "mins") {
      hrs = 0;
      mins = times;
    } else {
      hrs = times;
      mins = 0;
    }
    console.log(hrs, mins);

    let changedTime = this.sharedService.AddTime(hrs, mins);
    console.log(currentTime, "Current time");
    console.log(changedTime, "changed time");

    let obj = {
      crntTime: currentTime,
      orderVal: changedTime,
    };

    this.getOrderDeliveryTime(obj);
  }
}
