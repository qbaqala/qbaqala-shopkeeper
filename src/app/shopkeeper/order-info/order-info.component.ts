import { Component, OnInit } from "@angular/core";
import { AlertController, ModalController, NavParams, Platform } from "@ionic/angular";
import { ActivatedRoute, Router } from "@angular/router";
import { SharedService } from "src/app/services/shared.service";
import { AuthService } from "src/app/services/auth.service";
import { OrdersService } from "src/app/services/orders.service";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
declare var cordova;

@Component({
  selector: "app-order-info",
  templateUrl: "./order-info.component.html",
  styleUrls: ["./order-info.component.scss"],
})
export class OrderInfoComponent implements OnInit {
  photo_url: string = this.sharedService.Photopath;
  orderDetail: any = {};
  customerDetail: any = {};
  baqalaDetail: any = {};
  remainOrders: any = {};
  dispatchOrders: any = {};
  products: any = [];
  allItem: any = [];
  checkedItem: any = [];
  unchekedItem: any = [];
  itemCounts: any;
  totalPrice: any;
  uncheckedItemPrice: number = 0;
  checkedItemPrice: number = 0;
  totalOrders: Number;
  additionalItems: any;
  deliverDisatisfy: boolean = false;
  inProgress: boolean = false;
  productsForPlusIcon: any = [];

  constructor(
    private router: Router,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private sharedService: SharedService,
    private authService: AuthService,
    private orderService: OrdersService,
    private routes: ActivatedRoute,
    private alertCtrl: AlertController,
    public platform: Platform,
    private InAppBrowser: InAppBrowser,
    private clipboard: Clipboard
  ) {}

  ngOnInit() {
    let orderInfo = this.navParams.get("orderDetail");
    this.orderDetail = orderInfo.order;
    console.log(this.orderDetail, "order detail");
    if (this.orderDetail.status == "delivered" || this.orderDetail.status == "dissatisfied")
      this.deliverDisatisfy = true;
    if (this.orderDetail.subStatus == "preparing" || this.orderDetail.subStatus == "readyToDeliver")
      this.inProgress = true;
    let currentDate = new Date().toISOString().split(".");
    let orderExp = this.orderDetail.orderVal.split(".");
    // console.log(currentDate[0], " : current ", orderExp[0], " : exp date");
    if (orderExp[0] < currentDate[0] && this.orderDetail.status == "placed") {
      console.log(this.orderDetail, "Purged Orders");
      this.sharedService.setPurgedStatus(this.orderDetail._id, {
        status: "purged",
        orderPurged: new Date(),
      });
    }
    this.additionalItems = this.orderDetail.additionalItems;
    if (localStorage.getItem("baqalaDetail")) {
      this.verifyBaqala();
    }
    this.products = this.orderDetail.items;
    this.productsForPlusIcon = this.orderDetail.items;
    if (this.orderDetail.userId) {
      this.getUserDetail();
    }
    this.totalPrice = this.orderDetail.totalPrice;
    this.allItem = [];
    this.products.forEach((element) => {
      element.checked = true;
      this.allItem.push(element.itemQty);
    });
    this.itemCounts = this.sharedService.sumItem(this.allItem);
    console.log(this.products);
  }

  pressEvent() {
    if(this.customerDetail.phone_number){
      let num = this.customerDetail.countryCode + String(this.customerDetail.phone_number)
      this.clipboard.copy(num);
      alert("Copied to clipboard!")
    }
  }

  pressItem(){
    if(this.orderDetail.allItemsString){
      this.clipboard.copy(this.orderDetail.allItemsString);
      alert("Copied to clipboard!")
    }
  }

  goBack() {
    this.modalCtrl.dismiss();
  }

  checkFloat(value: number) {
    return Number.isInteger(value);
  }

  getUserDetail(notiData = null) {
    this.authService
      .getCurrentCustomer(this.orderDetail.userId)
      .then((res: any) => {
        this.customerDetail = res;
        console.log(this.customerDetail, "users detail");
        if (notiData === "dispatch") {
          let notiObj = {
            deviceId: this.customerDetail.deviceId,
            notiTitle: "Order Dispatched",
            notiDesc: "Order successfully dispatched",
          };
          if (this.customerDetail.deviceId)
            if (this.customerDetail.setting.orderDispatch === true)
              this.sharedService.sendNotification(notiObj);
        }
        if (notiData === "deliver") {
          let notiObj = {
            deviceId: this.customerDetail.deviceId,
            notiTitle: "Order Delivered",
            notiDesc: "Order successfully delivered",
          };
          if (this.customerDetail.deviceId)
            if (this.customerDetail.setting.orderDeliver == true)
              this.sharedService.sendNotification(notiObj);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  addItem(data) {
    console.log(this.products)
    console.log(this.productsForPlusIcon)
    console.log(this.allItem)
    console.log(data)
    if (data.checked == true) {
      data.itemQty = data.itemQty + 1;
      this.totalPrice = 0;
      this.allItem = [];
      this.products.forEach((element) => {
        if (element.checked == true) {
          this.totalPrice = this.totalPrice + element.customerPrice * element.itemQty;
          this.allItem.push(element.itemQty);
        }
      });
      console.log(this.allItem)
      this.itemCounts = this.sharedService.sumItem(this.allItem);
    } else {
      this.sharedService.simpleAlert("This item is not selected.");
    }
  }

  removeItem(data) {
    if (data.checked == true) {
      if (data.itemQty > 1) {
        data.itemQty = data.itemQty - 1;
        this.totalPrice = 0;
        this.allItem = [];
        this.products.forEach((element) => {
          if (element.checked == true) {
            this.totalPrice = this.totalPrice + element.customerPrice * element.itemQty;
            this.allItem.push(element.itemQty);
          }
        });
        this.itemCounts = this.sharedService.sumItem(this.allItem);
      } else {
        data.itemQty = 1;
      }
    } else {
      this.sharedService.simpleAlert("This item is not selected.");
    }
  }

  checkItem($event) {
    let checked = $event.target.checked;
    let checkedVal = $event.target.value;
    if (checked == false) {
      checkedVal.checked = false;
      this.totalPrice = this.totalPrice - checkedVal.customerPrice * checkedVal.itemQty;
      this.itemCounts = this.itemCounts - checkedVal.itemQty;
    } else {
      checkedVal.checked = true;
      this.totalPrice = this.totalPrice + checkedVal.customerPrice * checkedVal.itemQty;
      this.itemCounts = this.itemCounts + checkedVal.itemQty;
    }
  }

  verifyBaqala(val = null) {
    const user = JSON.parse(localStorage.getItem("baqalaDetail"));

    this.authService
      .getCurrentBaqala(user._id)
      .then((res) => {
        this.baqalaDetail = res;
        console.log(this.baqalaDetail, "Baqala Detail");
        if (this.baqalaDetail._id) {
          if (this.baqalaDetail.baqalaStatus == "Inactive" || this.baqalaDetail.isDeleted == true) {
            localStorage.removeItem("baqalaDetail");
            this.sharedService.simpleAlert("Baqala no longer active, please contact to qBaqala");
            this.modalCtrl.dismiss();
            this.router.navigate(["/identity"]);
          } else {
            localStorage.setItem("baqalaDetail", JSON.stringify(this.baqalaDetail));
            if (val == "dispatched") {
              if (this.orderDetail._id) {
                this.updateOrder(this.orderDetail._id, this.dispatchOrders);
              }
            } else if (val == "preparing") {
              this.updateOrderStatus(val);
            } else if (val == "readyToDeliver") {
              this.updateOrderStatus(val);
            } else if (val == "delivered") {
              this.setDeliveredStatus();
            }
          }
        } else {
          localStorage.removeItem("baqalaDetail");
          this.sharedService.simpleAlert("Unable to signin, please contact to qBaqala");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  updateOrder(id, data) {
    delete data.isActive;
    data.orderActive = false;

    this.orderService
      .updateOrder(id, data)
      .then((res: any) => {
        console.log(res, "order response get update");
        if (res.message) {
          console.log(res, "===Already dispatched===");
          this.sharedService.simpleAlert("Order no longer available");
          this.modalCtrl.dismiss(this.baqalaDetail);
          this.unchekedItem = [];
        } else {
          this.sharedService.titleAlert("Success", "Order dispatched successfully");
          console.log(res, "=== Success Dispatched ===");
          this.getUserDetail("dispatch");
          if (this.unchekedItem.length !== 0) {
            this.createOrder();
          }
          this.modalCtrl.dismiss(this.baqalaDetail);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  createOrder() {
    this.orderService
      .bookOrder(this.remainOrders)
      .then((res: any) => {
        console.log(res, "========== CREATE ORDER API IS CALLED =============");
        let notiObj = {
          deviceId: res.user.deviceId,
          notiTitle: "Order Placed",
          notiDesc: "Order successfully placed",
        };
        if (res.user.deviceId)
          if (res.user.setting.orderPlaced === true) this.sharedService.sendNotification(notiObj);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  dispatch() {
    this.unchekedItem = [];
    this.checkedItem = [];
    if (this.products.length > 0) {
      if (this.totalPrice < 1 || this.itemCounts < 1)
        return this.sharedService.simpleAlert("Please select any item to dispatch");

      this.products.forEach((element) => {
        if (element.checked == false) {
          this.unchekedItem.push(element);
          this.uncheckedItemPrice = 0;
          this.unchekedItem.forEach((element) => {
            this.uncheckedItemPrice =
              this.uncheckedItemPrice + element.customerPrice * element.itemQty;
            delete element.checked;
            delete element.isActive;
          });
        } else {
          this.checkedItem.push(element);
          this.checkedItemPrice = 0;
          this.checkedItem.forEach((element) => {
            this.checkedItemPrice = this.checkedItemPrice + element.customerPrice * element.itemQty;
            delete element.checked;
            delete element.isActive;
          });
        }
      });

      console.log(this.products, "products products");
      console.log(this.unchekedItem, "final unchecked item");
      console.log(this.checkedItem, "final checked item");

      // order done for checked
      this.dispatchOrders._id = this.orderDetail._id;
      this.dispatchOrders.createdAt = this.orderDetail.createdAt;
      this.dispatchOrders.isActive = this.orderDetail.isActive;
      this.dispatchOrders.orderDispatched = new Date();
      this.dispatchOrders.baqalaId = this.baqalaDetail._id;
      this.dispatchOrders.userId = this.customerDetail._id;
      this.dispatchOrders.totalPrice = this.checkedItemPrice;
      this.dispatchOrders.items = this.checkedItem;
      this.dispatchOrders.currency = this.orderDetail.currency;
      this.dispatchOrders.status = "dispatched";

      console.log(this.dispatchOrders, "fianl check order obj");

      this.remainOrders.countryCode = this.customerDetail.countryCode;
      this.remainOrders.phone_number = this.customerDetail.phone_number;
      this.remainOrders.currency = this.orderDetail.currency;
      if (this.customerDetail.fullName) {
        this.remainOrders.fullName = this.customerDetail.fullName;
      }
      if (this.customerDetail.latitude && this.customerDetail.longitude) {
        this.remainOrders.latitude = this.customerDetail.latitude;
        this.remainOrders.longitude = this.customerDetail.longitude;
      }
      this.remainOrders.orderPlaced = new Date();
      this.remainOrders.items = this.unchekedItem;
      this.remainOrders.totalPrice = this.uncheckedItemPrice;
      this.remainOrders.isPartialOrder = true;

      console.log(this.remainOrders, "final remain order");
      delete this.orderDetail.totalProducyQty;
      delete this.orderDetail.items;
      this.verifyBaqala("dispatched");
    } else {
      if (this.additionalItems) {
        this.dispatchOrders.orderDispatched = new Date();
        this.dispatchOrders.baqalaId = this.baqalaDetail._id;
        this.dispatchOrders.userId = this.customerDetail._id;
        this.dispatchOrders.currency = this.orderDetail.currency;
        this.dispatchOrders.status = "dispatched";
        this.onlyAdditionOrder();
      }
    }
  }

  onlyAdditionOrder() {
    this.verifyBaqala("dispatched");
  }

  goWhatsapp(){
    let url = "https://wa.me/"+ this.customerDetail.countryCode + this.customerDetail.phone_number
    // window.open(url, "_self");
    this.InAppBrowser.create(url, '_system');
  }

  async goToMap() {
    let baqalaLatLong = `${this.baqalaDetail.latitude},${this.baqalaDetail.longitude}`;
    let orderlaLatLong = `${this.orderDetail.latitude},${this.orderDetail.longitude}`;
    console.log(baqalaLatLong)
    console.log(orderlaLatLong)
    if(this.baqalaDetail.latitude && this.baqalaDetail.longitude && this.orderDetail.latitude && this.orderDetail.longitude){
      let modeVal = "driving";
      let url =
        "https://www.google.com/maps/dir/?api=1&travelmode=" +
        modeVal +
        "&layer=traffic&origin=" +
        baqalaLatLong +
        "&destination=" +
        orderlaLatLong;
      const alert = await this.alertCtrl.create({
        header: "Are you want to see details on map?",
        cssClass: "logout-alert",
        buttons: [
          {
            text: "CANCEL",
            role: "cancel",
          },
          {
            text: "OK",
            handler: () => {
              window.open(url, '_self');
              console.log("Confirm Okay");
            },
          },
        ],
      });
      await alert.present();
    }else{
      const alert = await this.alertCtrl.create({
        header: "Latitude longitude not available",
        cssClass: "logout-alert",
        buttons: [
          {
            text: "OK",
            handler: () => {
              console.log("Confirm Okay");
            },
          },
        ],
      });
      await alert.present();
    }
  }

  changeOrderStatus(data) {
    console.log("sending status to verify baqala == 1 ==", data);
    this.verifyBaqala(data);
  }

  delivered() {
    this.verifyBaqala("delivered");
  }

  setDeliveredStatus() {
    let orderObj = {
      orderDelivered: new Date(),
      status: "delivered",
    };
    this.orderService.updateOrderStatus(this.orderDetail._id, orderObj)
    .then((res) => {
      this.orderDetail = res;
      this.deliverDisatisfy = true;
      console.log(this.orderDetail, "order Delivered updated.");
      this.getUserDetail("deliver");
    })
    .catch((err) => {
      console.log(err);
    });
  }

  updateOrderStatus(status) {
    console.log(this.orderDetail, status + " : is coming");
    this.orderService.updateOrderStatus(this.orderDetail._id, { subStatus: status })
    .then((res) => {
      this.orderDetail = res;
      this.modalCtrl.dismiss();
      console.log(this.orderDetail, "order sub status updated.");
    })
    .catch((err) => {
      console.log(err);
    });
  }
}
