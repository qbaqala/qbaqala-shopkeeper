import { Component, OnInit } from "@angular/core";
import { ModalController, AlertController, IonRouterOutlet } from "@ionic/angular";
import { Router } from "@angular/router";
import { SharedService } from "../services/shared.service";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { EmailComposer } from "@ionic-native/email-composer/ngx";

@Component({
  selector: "app-shopkeeper",
  templateUrl: "./shopkeeper.page.html",
  styleUrls: ["./shopkeeper.page.scss"],
})
export class ShopkeeperPage implements OnInit {
  info = {
    fullName: "",
    phone_number: "",
  };
  menuArr: any = [
    {
      menuName: "Orders",
      imgUrl: "MyOrders.svg",
      linkActive: false,
      url: "/shopkeeper/",
    },
    {
      menuName: "Order Validity",
      imgUrl: "Order_validity.svg",
      linkActive: false,
      url: "/shopkeeper/setOrderValidity",
    },
    {
      menuName: "Delivery Time",
      imgUrl: "DeliveryTime.svg",
      linkActive: false,
      url: "/shopkeeper/setDeliveryTime",
    },
    // {
    //   menuName: "Notifications",
    //   imgUrl: "Notification.svg",
    //   linkActive: false,
    //   url: "/shopkeeper/setNotification",
    // },
  ];

  subMenuArr: any = [
    {
      menuName: "Default View Range",
      imgUrl: "default_view_range1.svg",
      linkActive: false,
      url: "/shopkeeper/viewRange",
    },
    {
      menuName: "Change Phone Number",
      imgUrl: "Mobile.svg",
      linkActive: false,
      url: "/shopkeeper/changeMobile",
    },
    // {
    //   menuName: "Contact Us",
    //   imgUrl: "help.svg",
    //   // url: "/shopkeeper/",
    //   linkActive: false,
    // },
    // {
    //   menuName: "Visit website",
    //   imgUrl: "compass.png",
    //   linkActive: false,
    //   url: "https://www.qbaqla.com/",
    // },
    // {
    //   menuName: "Logout",
    //   imgUrl: "Logout.svg",
    //   linkActive: false,
    //   url: "/shopkeeper/",
    // },
  ];

  subMenuArr2: any = [
    {
      menuName: "Visit website",
      imgUrl: "compass.png",
      linkActive: false,
      url: "https://www.qbaqla.com/",
    },
    {
      menuName: "Logout",
      imgUrl: "Logout.svg",
      linkActive: false,
      url: "/identity",
    },
  ];
  userDetail: any = {};
  appVersion: any;
  showContact: Boolean = false;
  callOption: Boolean = false;
  constructor(
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private router: Router,
    private sharedService: SharedService,
    private InAppBrowser: InAppBrowser,
    private clipboard: Clipboard,
    private emailComposer: EmailComposer,
    private routerOutlet: IonRouterOutlet
  ) {}

  ngOnInit() {
    this.routerOutlet.swipeGesture = false;
    this.menuArr[0].linkActive = true;
  }

  ionViewDidEnter() {
    if (localStorage.getItem("baqalaDetail")) {
      this.sharedService.getBaqalaInfo().subscribe((res) => {
        this.userDetail = res;
        if (!this.userDetail._id)
          this.userDetail = JSON.parse(localStorage.getItem("baqalaDetail"));
        else console.log(this.userDetail, "from baqala side");
      });
    }
    if (localStorage.getItem("appVersion")) {
      this.appVersion = JSON.parse(localStorage.getItem("appVersion"));
      console.log(this.appVersion, "====appVersion====");
    }
  }

  selectedMenu(data) {
    this.subMenuArr.forEach((element) => {
      element.linkActive = false;
    });
    this.menuArr.forEach((element) => {
      if (element.menuName == data.menuName) {
        element.linkActive = true;
      } else {
        element.linkActive = false;
      }
    });
  }

  async selectedSubMenu(data) {
    this.router.navigateByUrl(data.url)
    this.menuArr.forEach((element) => {
      element.linkActive = false;
    });
    this.subMenuArr.forEach(async (element) => {
      if (element.menuName == data.menuName) {
        element.linkActive = true;
        if (data.menuName == "Contact Us") {
          // this.showContact = !this.showContact
          // window.open("tel:+97477536322", "_self");
        }
        if (data.menuName == "Visit website") {
          let url = "https://www.qbaqala.com";
          this.InAppBrowser.create(url, '_system');
          // window.open(url, "_self");
        }
        if (data.menuName == "Logout") {
          const alert = await this.alertCtrl.create({
            cssClass: "logout-alert",
            header: "Are you sure you want to logout?",
            message: "You will not receive any update in the app",
            buttons: [
              {
                text: "Cancel",
                role: "cancel",
                cssClass: "secondary",
                handler: (blah) => {
                  console.log("Confirm Cancel: blah");
                },
              },
              {
                text: "Logout",
                handler: () => {
                  localStorage.removeItem("baqalaDetail");
                  localStorage.clear();
                  this.router.navigate(["/identity"]);
                  this.sharedService.updateBaqalaDetail(this.info);
                  this.subMenuArr.forEach((element) => {
                    element.linkActive = false;
                  });
                  console.log("Confirm Okay");
                },
              },
            ],
          });
          await alert.present();
        }
      } else {
        element.linkActive = false;
      }
    });
  }

  visitWebsite(){
    let url = "https://www.qbaqala.com";
    window.open(url, "_blank");
    this.InAppBrowser.create(url, '_system');
  }

  async logout(){
    const alert = await this.alertCtrl.create({
      cssClass: "logout-alert",
      header: "Are you sure you want to logout?",
      message: "You will not receive any update in the app",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Logout",
          handler: () => {
            localStorage.removeItem("baqalaDetail");
            localStorage.clear();
            this.router.navigate(["/identity"]);
            this.sharedService.updateBaqalaDetail(this.info);
            this.subMenuArr.forEach((element) => {
              element.linkActive = false;
            });
            console.log("Confirm Okay");
          },
        },
      ],
    });
    await alert.present();
  }

  contDta() {
    this.showContact = !this.showContact
  }

  contactUs() {
    this.callOption = !this.callOption
  }

  forIndia(){
    window.open("tel:+919560447688", "_self");
  }

  forOatar(){
    window.open("tel:+97477536322", "_self");
  }

  goWhatsapp(){
    // let url = "https://wa.me/message/UBJWZI5YNGNRL1"
    // let url = "https://wa.me/+97477536322"
    // this.InAppBrowser.create(url, '_system');
    if(this.userDetail.countryCode == "+91"){
      let url = "https://wa.me/+919560447688";
      this.InAppBrowser.create(url, '_system');
    }else{
      let url = "https://wa.me/+97477536322";
      this.InAppBrowser.create(url, '_system');
    }
  }

  copyEmail() {
    this.clipboard.copy("support@baqala.com");
    this.sharedService.simpleAlert("Copied to clipboard!");
    // alert("Copied to clipboard!");
  }

  sendEmail() {
    let email = {
      to: 'support@baqala.com',
      // subject: " need help",
      // body: this.emailData.message,
      isHtml: false
    }
    this.emailComposer.open(email);
  }

}
