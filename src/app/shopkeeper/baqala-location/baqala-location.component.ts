import { Component, OnInit, ElementRef, NgZone, ViewChild } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import {
  NativeGeocoder,
  NativeGeocoderResult,
  NativeGeocoderOptions,
} from "@ionic-native/native-geocoder/ngx";
import { ModalController } from "@ionic/angular";
import { Router } from "@angular/router";
import { SharedService } from "src/app/services/shared.service";
import { AuthService } from "src/app/services/auth.service";

declare var google;

@Component({
  selector: "app-baqala-location",
  templateUrl: "./baqala-location.component.html",
  styleUrls: ["./baqala-location.component.scss"],
})
export class BaqalaLocationComponent implements OnInit {
  @ViewChild("map", { static: false }) mapElement: ElementRef;

  map: any;
  address: string;
  newaddress: string;
  lat: string;
  long: string;
  autocomplete: { input: string };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  GoogleAutocomplete: any;
  baqalaDetail: any = {};
  curntLatlong: any = {};

  constructor(
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public zone: NgZone,
    private router: Router,
    private sharedService: SharedService,
    private authService: AuthService
  ) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: "" };
    this.autocompleteItems = [];
  }

  ngOnInit() {
    setTimeout(() => {
      this.loadMap();
    }, 1000);
  }

  ionViewDidEnter() {
    // this.sharedService.getBaqalaInfo().subscribe((res) => {
    //   this.baqalaDetail = res;
    //   console.log(this.baqalaDetail, "current Baqala");
    // });

    if (localStorage.getItem("baqalaDetail")) {
      this.baqalaDetail = JSON.parse(localStorage.getItem("baqalaDetail"));
      if (this.baqalaDetail._id) console.log(this.baqalaDetail, "current Baqala");
    }
  }

  goBack() {
    this.router.navigate(["/shopkeeper"]);
  }

  loadMap() {
    //FIRST GET THE LOCATION FROM THE DEVICE.
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          // disableDefaultUI: true,
          // draggable: true,
        };

        //LOAD THE MAP WITH THE PREVIOUS VALUES AS PARAMETERS.
        this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.map.addListener("tilesloaded", () => {
          console.log("accuracy", this.map, this.map.center.lat());
          this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng());
          this.lat = this.map.center.lat();
          this.long = this.map.center.lng();
        });
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }

  addMarker(lat, long) {
    let position = new google.maps.LatLng(lat, long);
    let mapMarkers = new google.maps.Marker({
      position: position,
      animation: google.maps.Animation.DROP,
      icon: {
        url: "../../../assets/imgs/shop.png",
        scaledSize: new google.maps.Size(54, 54),
      },
    });
    mapMarkers.setMap(this.map);
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords " + lattitude + " " + longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5,
    };

    this.curntLatlong = {
      latitude: lattitude,
      longitude: longitude,
    };

    this.nativeGeocoder
      .reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log(result, "result");
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if (value.length > 0) responseAddress.push(value);
        }
        console.log(responseAddress, "resAddress");
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value + ", ";
        }
        this.address = this.address.slice(0, -2);
        this.newaddress = this.generateAddress(result[0]);
        console.log(this.address, "mai add");
      })
      .catch((error: any) => {
        this.address = "Address Not Available!";
      });
  }

  generateAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length) address += obj[val] + ", ";
    }
    return address.slice(0, -2);
  }

  saveNewLocation() {
    this.baqalaDetail.latitude = this.curntLatlong.latitude;
    this.baqalaDetail.longitude = this.curntLatlong.longitude;
    this.baqalaDetail.updatedAt = new Date();
    this.authService
      .updateBaqala(this.baqalaDetail._id, this.baqalaDetail)
      .then((res) => {
        console.log(res, "new location saved");
        localStorage.setItem("baqalaDetail", JSON.stringify(res));
        this.sharedService.updateBaqalaDetail(res);
        this.sharedService.titleAlert("Success", "New location saved successfully.");
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
