import { Component, OnInit } from "@angular/core";
import { SharedService } from "src/app/services/shared.service";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";

@Component({
  selector: "app-change-mobile",
  templateUrl: "./change-mobile.component.html",
  styleUrls: ["./change-mobile.component.scss"],
})
export class ChangeMobileComponent implements OnInit {
  userDetail: any = {};
  apiRoute: any = {};
  isSubmit: boolean = false;
  formValidate: FormGroup;
  baqalaInfo: any = {};

  constructor(
    private sharedService: SharedService,
    private authService: AuthService,
    private router: Router,
    public fb: FormBuilder
  ) {
    if (localStorage.getItem("baqalaDetail")) {
      this.sharedService.getBaqalaInfo().subscribe((res) => {
        this.userDetail = res;
        if (!this.userDetail) {
          this.userDetail = JSON.parse(localStorage.getItem("baqalaDetail"));
        } else {
        }
      });
      if(this.userDetail.countryCode == "+91"){
        this.formValidate = this.fb.group({
          phone_number: [
            this.userDetail.phone_number,
            [
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(10),
              Validators.pattern("^[0-9]*$"),
            ],
          ],
        });

      }else{
        this.formValidate = this.fb.group({
          phone_number: [
            this.userDetail.phone_number,
            [
              Validators.required,
              Validators.minLength(8),
              Validators.maxLength(8),
              Validators.pattern("^[0-9]*$"),
            ],
          ],
        });
      }
    }
  }

  ngOnInit() {}

  goBack() {
    this.router.navigate(["/shopkeeper"]);
  }

  changeNumber() {
    this.isSubmit = true;
    if (this.formValidate.valid) {
      console.log(this.formValidate.value);
      this.formValidate.value.updatedAt = new Date();
      this.authService.updateBaqalaPhone(this.userDetail._id, this.formValidate.value).then(
        (data) => {
          console.log(data);
          this.baqalaInfo = data;
          if (this.baqalaInfo.message) {
            this.sharedService.titleAlert("Alert!!", this.baqalaInfo.message);
          } else {
            localStorage.setItem("baqalaDetail", JSON.stringify(data));
            this.sharedService.updateBaqalaDetail(data);
            this.sharedService.simpleAlert("Phone number is updated successfully");
          }
        },
        (err) => {
          console.log(err);
          if (err.error) {
            this.sharedService.simpleAlert(err.error.reason);
          }
        }
      );
    } else {
      if(this.userDetail.countryCode == "+91"){
        this.sharedService.simpleAlert("Please enter phone number, should be 10 digits");
      }else{
        this.sharedService.simpleAlert("Please enter phone number, should be 8 digits");
      }
    }
  }
}
