import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetOrderValidityComponent } from './set-order-validity.component';

describe('SetOrderValidityComponent', () => {
  let component: SetOrderValidityComponent;
  let fixture: ComponentFixture<SetOrderValidityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetOrderValidityComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetOrderValidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
