import { Component, OnInit, ViewChild } from "@angular/core";
import { IonSlides, ModalController } from "@ionic/angular";
import { Router } from "@angular/router";
import { hours, minutes } from "../../constants/times";
import { SharedService } from "src/app/services/shared.service";
import { OrdersService } from "src/app/services/orders.service";

@Component({
  selector: "app-set-order-validity",
  templateUrl: "./set-order-validity.component.html",
  styleUrls: ["./set-order-validity.component.scss"],
})
export class SetOrderValidityComponent implements OnInit {
  @ViewChild("hourSlide", { static: false }) hourSlide: IonSlides;
  @ViewChild("minuteSlide", { static: false }) minuteSlide: IonSlides;

  orderVal: any;
  crntTime: any;
  hours: any;
  minutes: any;
  totalTime: any;
  hoursArr: any = hours;
  minutesArr: any = minutes;
  constructor(
    private router: Router,
    private sharedService: SharedService,
    private orderService: OrdersService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {}

  slideOptsOne = {
    initialSlide: 2,
    slidesPerView: 5,
    spaceBetween: 0,
    centeredSlides: true,
    direction: "vertical",
  };

  goBack() {
    this.router.navigate(["/shopkeeper"]);
  }

  getOrderByTime() {}

  hourChanged() {
    this.hourSlide.getActiveIndex().then((index) => {
      console.log(index);
      let currentIndex = index;
      let objVal = this.hoursArr[currentIndex];
      // console.log(currentIndex);
      this.hoursArr.forEach((element) => {
        if (element.hrs == objVal.hrs) {
          element.isActive = true;
          // console.log(element, "changed");
          this.hours = element.hrs;
        } else {
          element.isActive = false;
        }
      });
    });
  }

  minuteChanged() {
    this.minuteSlide
      .getActiveIndex()
      .then((index) => {
        console.log(index);
        let currentIndex = index;
        let objVal = this.minutesArr[currentIndex];
        // console.log(currentIndex);
        this.minutesArr.forEach((element) => {
          if (element.minute == objVal.minute) {
            element.isActive = true;
            // console.log(element, "changed");
            this.minutes = element.minute;
          } else {
            element.isActive = false;
          }
        });
      })
      .catch((err) => {
        console.log(err, "error");
      });
  }

  saveValidity() {
    console.log(this.hours, this.minutes, " : Selected time");
    let hrs = parseInt(this.hours);
    let mnt = parseInt(this.minutes);
    let currentTime = new Date();
    let orderValidity;
    orderValidity = this.sharedService.AddTime(hrs, mnt);
    let obj = {
      crntTime: currentTime,
      orderVal: orderValidity,
    };

    console.log(obj, "final");

    this.orderService
      .getOrderByOrderValidity(obj)
      .then((res) => {
        console.log(res);
        this.sharedService.titleAlert("Success", "Order fetched successfully.");
        this.router.navigate(["/shopkeeper"]);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
