import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ShopkeeperPage } from "./shopkeeper.page";
import { ShopsComponent } from "./shops/shops.component";
import { SetOrderValidityComponent } from "./set-order-validity/set-order-validity.component";
import { SetDeliveryTimeComponent } from "./set-delivery-time/set-delivery-time.component";
import { DefaultViewRangeComponent } from "./default-view-range/default-view-range.component";
import { BaqalaLocationComponent } from "./baqala-location/baqala-location.component";
import { ChangeMobileComponent } from "./change-mobile/change-mobile.component";
// import { SetNotificationComponent } from "./set-notification/set-notification.component";

const routes: Routes = [
  {
    path: "",
    component: ShopkeeperPage,
    children: [
      {
        path: "",
        component: ShopsComponent,
      },
      {
        path: "setOrderValidity",
        component: SetOrderValidityComponent,
      },
      {
        path: "setDeliveryTime",
        component: SetDeliveryTimeComponent,
      },
      // {
      //   path: "setNotification",
      //   component: SetNotificationComponent,
      // },
      {
        path: "viewRange",
        component: DefaultViewRangeComponent,
      },
      {
        path: "setLocation",
        component: BaqalaLocationComponent,
      },
      {
        path: "changeMobile",
        component: ChangeMobileComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopkeeperPageRoutingModule {}
