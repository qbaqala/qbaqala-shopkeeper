import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ShopkeeperPageRoutingModule } from "./shopkeeper-routing.module";

import { ShopkeeperPage } from "./shopkeeper.page";
import { ShopsComponent } from "./shops/shops.component";
import { OrderInfoComponent } from "./order-info/order-info.component";
import { SetOrderValidityComponent } from "./set-order-validity/set-order-validity.component";
import { DefaultViewRangeComponent } from "./default-view-range/default-view-range.component";
import { SelectDistanceComponent } from "./select-distance/select-distance.component";
import { SetDeliveryTimeComponent } from "./set-delivery-time/set-delivery-time.component";
import { BaqalaLocationComponent } from "./baqala-location/baqala-location.component";
import { ChangeMobileComponent } from "./change-mobile/change-mobile.component";
import { AlertPopupComponent } from "./alert-popup/alert-popup.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ShopkeeperPageRoutingModule,
  ],
  declarations: [
    ShopkeeperPage,
    ShopsComponent,
    OrderInfoComponent,
    SetOrderValidityComponent,
    SetDeliveryTimeComponent,
    DefaultViewRangeComponent,
    SelectDistanceComponent,
    BaqalaLocationComponent,
    ChangeMobileComponent,
    AlertPopupComponent
  ],
})
export class ShopkeeperPageModule {}
