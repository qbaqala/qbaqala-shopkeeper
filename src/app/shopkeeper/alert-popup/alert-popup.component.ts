import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-alert-popup',
  templateUrl: './alert-popup.component.html',
  styleUrls: ['./alert-popup.component.scss'],
})
export class AlertPopupComponent implements OnInit {
  closeClk: boolean = false;
  showMsg: string;
  dataObj: any = {};
  constructor(
    public modalCtrl: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    if (this.navParams.get("userReqData")) {
      this.dataObj = this.navParams.get("userReqData");
      console.log(this.dataObj);
      if(this.dataObj.message){
        this.showMsg = "Number is not registered. Please enter a valid number or register as new baqala.";
      }else{
        this.showMsg = "Thank you. qBaqala will contact you shortly with your new password.";
      }
    }
  }

  close() {
    this.modalCtrl.dismiss();
    this.closeClk = true;
    console.log(this.closeClk);
  }

}