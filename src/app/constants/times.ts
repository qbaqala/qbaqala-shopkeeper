export const hours = [
  {
    hrs: "01",
    isActive: false,
  },
  {
    hrs: "02",
    isActive: false,
  },
  {
    hrs: "03",
    isActive: false,
  },
  {
    hrs: "04",
    isActive: false,
  },
  {
    hrs: "05",
    isActive: false,
  },
  {
    hrs: "06",
    isActive: false,
  },
  {
    hrs: "07",
    isActive: false,
  },
  {
    hrs: "08",
    isActive: false,
  },
  {
    hrs: "09",
    isActive: false,
  },
  {
    hrs: "10",
    isActive: false,
  },
  {
    hrs: "11",
    isActive: false,
  },
  {
    hrs: "12",
    isActive: false,
  },
  {
    hrs: "13",
    isActive: false,
  },
  {
    hrs: "14",
    isActive: false,
  },
  {
    hrs: "15",
    isActive: false,
  },
  {
    hrs: "16",
    isActive: false,
  },
  {
    hrs: "17",
    isActive: false,
  },
  {
    hrs: "18",
    isActive: false,
  },
  {
    hrs: "19",
    isActive: false,
  },
  {
    hrs: "20",
    isActive: false,
  },
  {
    hrs: "21",
    isActive: false,
  },
  {
    hrs: "22",
    isActive: false,
  },
  {
    hrs: "23",
    isActive: false,
  },
];

export const minutes = [
  {
    minute: "00",
    isActive: false,
  },
  {
    minute: "01",
    isActive: false,
  },
  {
    minute: "02",
    isActive: false,
  },
  {
    minute: "03",
    isActive: false,
  },
  {
    minute: "04",
    isActive: false,
  },
  {
    minute: "05",
    isActive: false,
  },
  {
    minute: "06",
    isActive: false,
  },
  {
    minute: "07",
    isActive: false,
  },
  {
    minute: "08",
    isActive: false,
  },
  {
    minute: "09",
    isActive: false,
  },
  {
    minute: "10",
    isActive: false,
  },
  {
    minute: "11",
    isActive: false,
  },
  {
    minute: "12",
    isActive: false,
  },
  {
    minute: "13",
    isActive: false,
  },
  {
    minute: "14",
    isActive: false,
  },
  {
    minute: "15",
    isActive: false,
  },
  {
    minute: "16",
    isActive: false,
  },
  {
    minute: "17",
    isActive: false,
  },
  {
    minute: "18",
    isActive: false,
  },
  {
    minute: "19",
    isActive: false,
  },
  {
    minute: "20",
    isActive: false,
  },
  {
    minute: "21",
    isActive: false,
  },
  {
    minute: "22",
    isActive: false,
  },
  {
    minute: "23",
    isActive: false,
  },
  {
    minute: "24",
    isActive: false,
  },
  {
    minute: "25",
    isActive: false,
  },
  {
    minute: "26",
    isActive: false,
  },
  {
    minute: "27",
    isActive: false,
  },
  {
    minute: "28",
    isActive: false,
  },
  {
    minute: "29",
    isActive: false,
  },
  {
    minute: "30",
    isActive: false,
  },
  {
    minute: "31",
    isActive: false,
  },
  {
    minute: "32",
    isActive: false,
  },
  {
    minute: "33",
    isActive: false,
  },
  {
    minute: "34",
    isActive: false,
  },
  {
    minute: "35",
    isActive: false,
  },
  {
    minute: "36",
    isActive: false,
  },
  {
    minute: "37",
    isActive: false,
  },
  {
    minute: "38",
    isActive: false,
  },
  {
    minute: "39",
    isActive: false,
  },
  {
    minute: "40",
    isActive: false,
  },
  {
    minute: "41",
    isActive: false,
  },
  {
    minute: "42",
    isActive: false,
  },
  {
    minute: "43",
    isActive: false,
  },
  {
    minute: "44",
    isActive: false,
  },
  {
    minute: "45",
    isActive: false,
  },
  {
    minute: "46",
    isActive: false,
  },
  {
    minute: "47",
    isActive: false,
  },
  {
    minute: "48",
    isActive: false,
  },
  {
    minute: "49",
    isActive: false,
  },
  {
    minute: "50",
    isActive: false,
  },
  {
    minute: "51",
    isActive: false,
  },
  {
    minute: "52",
    isActive: false,
  },
  {
    minute: "53",
    isActive: false,
  },
  {
    minute: "54",
    isActive: false,
  },
  {
    minute: "55",
    isActive: false,
  },
  {
    minute: "56",
    isActive: false,
  },
  {
    minute: "57",
    isActive: false,
  },
  {
    minute: "58",
    isActive: false,
  },
  {
    minute: "59",
    isActive: false,
  },
];
