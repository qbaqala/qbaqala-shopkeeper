import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import { AlertController, IonRouterOutlet, Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Router } from "@angular/router";
import { AuthService } from "./services/auth.service";
import { SharedService } from "./services/shared.service";
import { AppVersion } from "@ionic-native/app-version/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  public hasPermission: boolean;
  public token: string;
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  baqalaDetail: any = {};
  AppName:string;
  PackageName:string;
  VersionCode:string|number;
  VersionNumber:string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authService: AuthService,
    private sharedService: SharedService,
    private appVersion: AppVersion,
    public alertCtrl: AlertController,
    private InAppBrowser: InAppBrowser
  ) {
    this.initializeApp();
    this.verifyBaqala();
    if (localStorage.getItem("baqalaDetail")) {
      this.router.navigate(["/shopkeeper/"]);
    } else {
      this.router.navigate(["/identity"]);
    }
    this.backButtonEvent();
  }

  ngOnInit() {}

  backButtonEvent() {
    this.platform.ready().then(() => {
      this.platform.backButton.subscribeWithPriority(9999, () => {
        document.addEventListener(
          "backbutton",
          function (event) {
            event.preventDefault();
            event.stopPropagation();
            // alert("back button is disabled");
          },
          false
        );
      });
    });
  }

  verifyBaqala() {
    if (localStorage.getItem("baqalaDetail")) {
      const user = JSON.parse(localStorage.getItem("baqalaDetail"));
      this.authService
        .getCurrentBaqala(user._id)
        .then((res) => {
          this.baqalaDetail = res;
          if (this.baqalaDetail._id) {
            if (
              this.baqalaDetail.baqalaStatus == "Inactive" ||
              this.baqalaDetail.isDeleted == true
            ) {
              localStorage.removeItem("baqalaDetail");
              this.sharedService.simpleAlert("Unable to signin, please contact to qBaqala");
              this.router.navigate(["/identity"]);
            } else {
              localStorage.setItem("baqalaDetail", JSON.stringify(this.baqalaDetail));
            }
          } else {
            localStorage.removeItem("baqalaDetail");
            this.sharedService.simpleAlert("Unable to signin, please contact to qBaqala");
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        this.statusBar.styleLightContent();
        this.statusBar.overlaysWebView(false);
        this.statusBar.backgroundColorByHexString("#000000");
        this.checkUpdateAndroid();
      }
      if (this.platform.is("ios")) {
        this.statusBar.styleLightContent();
        this.statusBar.overlaysWebView(false);
        this.statusBar.backgroundColorByHexString("#000000");
        this.checkUpdateIos();
      }
    });
  }

  checkUpdateAndroid(){
    this.authService.getVersion()
    .then((res:any) => {
      if(res){
        this.getVersion(res.shopkeeperAndroid);
      }
    })
    .catch((err) => {
      console.log(err);
    });
  }

  checkUpdateIos(){
    this.authService.getVersion()
    .then((res:any) => {
      if(res){
        this.getVersionIos(res.shopkeeperIos);
      }
    })
    .catch((err) => {
      console.log(err);
    });
  }

  getVersion(res){
    this.appVersion.getVersionNumber().then(value => {
      this.VersionNumber = value;
      console.log(this.VersionNumber,"====VersionNumber====");
      localStorage.setItem("appVersion", JSON.stringify(this.VersionNumber));
      console.log(res,"====resVersion====");
      if(res > this.VersionNumber){
        this.alert()
      }
    }).catch(err => {
      alert(err);
    });
  }

  getVersionIos(res){
    this.appVersion.getVersionNumber().then(value => {
      this.VersionNumber = value;
      console.log(this.VersionNumber,"====VersionNumber====");
      localStorage.setItem("appVersion", JSON.stringify(this.VersionNumber));
      console.log(res,"====resVersion====");
      if(res > this.VersionNumber){
        this.alertIos()
      }
    }).catch(err => {
      alert(err);
    });
  }

  async alert(){
    const alert = await this.alertCtrl.create({
      cssClass: "logout-alert",
      header: "Update available",
      message: "There is a new version available on the Store",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Yes",
          handler: () => {
            window.open("https://play.google.com/store/apps/details?id=com.qBaqalaShop.io", '_self');
            console.log("Confirm Okay");
          },
        },
      ],
    });
    await alert.present();
  }

  async alertIos(){
    const alert = await this.alertCtrl.create({
      cssClass: "logout-alert",
      header: "Update available",
      message: "There is a new version available on the Store",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Yes",
          handler: () => {
            // window.open("https://apps.apple.com/in/app/qbaqala-shopkeeper/id1554290273", '_self');
            this.InAppBrowser.create("https://apps.apple.com/in/app/qbaqala-shopkeeper/id1554290273", '_system', 'location=yes');
            console.log("Confirm Okay");
          },
        },
      ],
    });
    await alert.present();
  }

}
