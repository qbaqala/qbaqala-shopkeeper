import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { IdentityPage } from "./identity.page";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";

const routes: Routes = [
  {
    path: "",
    component: IdentityPage,
    children: [
      { path: "", component: SigninComponent },
      { path: "signup", component: SignupComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IdentityPageRoutingModule {}
