import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { IdentityPageRoutingModule } from "./identity-routing.module";

import { IdentityPage } from "./identity.page";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, IdentityPageRoutingModule, ReactiveFormsModule],
  declarations: [IdentityPage, SigninComponent, SignupComponent],
})
export class IdentityPageModule {}
