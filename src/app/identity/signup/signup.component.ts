import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ModalController } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { SharedService } from "src/app/services/shared.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
  signupObj: any = {};
  showSuccess = false;
  isSubmit: boolean = false;
  formValidate: FormGroup;
  countryCode: any = '';
  errMsg: Boolean = false;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService,
    private router: Router,
    public modalCtrl: ModalController,
    public fb: FormBuilder
  ) {
    this.formValidate = this.fb.group({
      fullName: ["", [Validators.required, Validators.minLength(3)]],
      countryCode: ['', [Validators.required]],
      phone_number: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(8),
          Validators.pattern("^[0-9]*$"),
        ],
      ],
    });
  }

  ngOnInit() {}

  get valid() {
    return this.formValidate.controls;
  }

  selectCode(event) {
    console.log(event)
    this.countryCode = event.detail.value;
    if(this.countryCode == '+91' ){
      console.log("InInr");
      this.formValidate = this.fb.group({
        fullName: [this.formValidate.value.fullName, [Validators.required, Validators.minLength(3)]],
        countryCode: [this.countryCode, [Validators.required]],
        phone_number: [
          this.formValidate.value.phone_number,
          [
            Validators.minLength(10),
            Validators.maxLength(10),
            Validators.pattern("^[0-9]*$"),
            Validators.required,
          ],
        ]
      });
    }
    if(this.countryCode == '+974' ){
      console.log("Inqar");
      this.formValidate = this.fb.group({
        fullName: [this.formValidate.value.fullName, [Validators.required, Validators.minLength(3)]],
        countryCode: [this.countryCode, [Validators.required]],
        phone_number: [
          this.formValidate.value.phone_number,
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(8),
            Validators.pattern("^[0-9]*$"),
          ],
        ]
      });
    }
  }

  async signup() {
    this.isSubmit = true;
    if(this.formValidate.value.phone_number != null){
      let num = this.formValidate.value.phone_number.toString().length
      if(this.countryCode == '+91' ){
        if(num == 10){
          this.errMsg = false;
          this.signupCorrect();
        }else{
          this.errMsg = true;
        }
      }
      if(this.countryCode == '+974' ){
        if(num == 8){
          this.errMsg = false;
          this.signupCorrect();
        }else{
          this.errMsg = true;
        }
      }
    }
  }

  signupCorrect() {
    this.isSubmit = true;
    if (this.formValidate.valid) {
      console.log(this.formValidate.value);
      this.authService
        .baqalaSignup(this.formValidate.value)
        .then((res) => {
          console.log(res);
          this.signupObj = res;
          if (this.signupObj.message) {
            this.sharedService.simpleAlert(this.signupObj.message);
          } else {
            this.showSuccess = true;
            setTimeout(() => {
              this.modalCtrl.dismiss();
              this.router.navigate(["/identity"]);
            }, 2000);
          }
        })
        .catch((err) => {
          console.log(err.error);
          if (err.error) {
            this.sharedService.simpleAlert(err.error.reason);
          }
        });
    }
  }

  signin() {
    this.router.navigate(["/"]);
    this.modalCtrl.dismiss();
  }
}
