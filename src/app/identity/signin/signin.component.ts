import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IonRouterOutlet, ModalController } from "@ionic/angular";
import { SignupComponent } from "../signup/signup.component";
import { AuthService } from "src/app/services/auth.service";
import { SharedService } from "src/app/services/shared.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { AlertPopupComponent } from "src/app/shopkeeper/alert-popup/alert-popup.component";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"],
})
export class SigninComponent implements OnInit {
  loginObj: any = {
    phone_number: null,
    password: "",
  };
  isSubmit: boolean = false;
  formValidate: FormGroup;
  formValidateForgot: FormGroup;
  forgotPass: boolean = false;
  isRequest: boolean = false;
  countryCode: any = '';
  errMsg: Boolean = false;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService,
    public fb: FormBuilder,
    public modalCtrl: ModalController,
    private router: Router,
    private routerOutlet: IonRouterOutlet
  ) {
    this.formValidate = this.fb.group({
      countryCode: ["", [Validators.required]],
      phone_number: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(8),
          Validators.pattern("^[0-9]*$"),
        ],
      ],
      password: ["", [Validators.required]],
    });

    this.formValidateForgot = this.fb.group({
      countryCode: ["", [Validators.required]],
      phone_number: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(8),
          Validators.pattern("^[0-9]*$"),
        ]
      ]
    });
  }

  ngOnInit() {
    this.routerOutlet.swipeGesture = false;
  }

  get valid() {
    return this.formValidate.controls;
  }

  get validReq() {
    return this.formValidateForgot.controls;
  }

  selectCode(event) {
    console.log(event)
    this.countryCode = event.detail.value;
    if(this.countryCode == '+91' ){
      console.log("InInr");
      this.formValidate = this.fb.group({
        countryCode: [this.countryCode, [Validators.required]],
        phone_number: [
          this.formValidate.value.phone_number,
          [
            Validators.minLength(10),
            Validators.maxLength(10),
            Validators.pattern("^[0-9]*$"),
            Validators.required,
          ],
        ],
        password: ["", [Validators.required]],
      });
    }
    if(this.countryCode == '+974' ){
      console.log("Inqar");
      this.formValidate = this.fb.group({
        countryCode: [this.countryCode, [Validators.required]],
        phone_number: [
          this.formValidate.value.phone_number,
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(8),
            Validators.pattern("^[0-9]*$"),
          ],
        ],
        password: ["", [Validators.required]],
      });
    }
  }

  async signin() {
    this.isSubmit = true;
    if(this.formValidate.value.phone_number != null){
      let num = this.formValidate.value.phone_number.toString().length
      if(this.countryCode == '+91' ){
        if(num == 10){
          this.errMsg = false;
          this.signinCorrect();
        }else{
          this.errMsg = true;
        }
      }
      if(this.countryCode == '+974' ){
        if(num == 8){
          this.errMsg = false;
          this.signinCorrect();
        }else{
          this.errMsg = true;
        }
      }
    }
  }

  signinCorrect() {
    this.isSubmit = true;
    if (this.formValidate.valid) {
      console.log(this.formValidate.value);
      this.authService
      .baqalaSignin(this.formValidate.value)
      .then((res) => {
        this.loginObj = res;
        if (this.loginObj.isDeleted == true || this.loginObj.baqalaStatus == "Inactive") {
          this.loginObj = {};
          return this.sharedService.simpleAlert("Unable to signin, please contact to qBaqala");
        }
        console.log(res);
        localStorage.setItem("baqalaDetail", JSON.stringify(res));
        this.sharedService.updateBaqalaDetail(res);
        this.router.navigate(["/shopkeeper"], {replaceUrl: true});
        if (localStorage.getItem("TokenDevicId")) {
          this.updateDeviceId();
        }
      })
      .catch((err) => {
        console.log(err);
        if (err.error) {
          this.sharedService.simpleAlert(err.error.reason);
        }
      });
    }
  }

  updateDeviceId() {
    this.loginObj.deviceId = localStorage.getItem("TokenDevicId");
    this.authService.updateBaqala(this.loginObj._id, this.loginObj)
    .then((res) => {
      localStorage.setItem("baqalaDetail", JSON.stringify(res));
      this.sharedService.updateBaqalaDetail(res);
      console.log(res, "device id updated");
      this.loginObj = {};
    })
    .catch((err) => {
      console.log(err);
    });
  }

  async signup() {
    const modal = await this.modalCtrl.create({
      component: SignupComponent,
      // componentProps: { mealData: data },
    });
    return await modal.present();
  }

  forgot(){
    this.forgotPass = !this.forgotPass;
  }

  request(){
    let num = this.formValidate.value.phone_number.toString().length
    if(num == 0){
      this.isRequest = true;
      // if (this.formValidateForgot.valid) {
        console.log(this.formValidate.value);
        this.authService
        .baqalaForgotReq(this.formValidate.value)
        .then((res:any) => {
          console.log(res);
          // this.forgotPass = false;
          if(res.message){
            console.log("In if");
          }else{
            this.forgotPass = false;
            console.log("In else");
          }
          this.confirmPopup(res);
        })
        .catch((err) => {
          console.log(err);
          if (err.error) {
            this.sharedService.simpleAlert(err.error.reason);
          }
        });
      // }
    }else{
      this.sharedService.simpleAlert("Please enter phone number");
    }
  }

  async confirmPopup(data) {
    const modal = await this.modalCtrl.create({
      component: AlertPopupComponent,
      cssClass: "image-modal thanks-modal",
      componentProps: { userReqData: data },
    });
    return await modal.present();
  }

}
