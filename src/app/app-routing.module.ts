import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
  {
    path: "identity",
    loadChildren: () => import("./identity/identity.module").then((m) => m.IdentityPageModule),
    // canActivate: [AuthGuard],
  },
  {
    path: "shopkeeper",
    loadChildren: () =>
      import("./shopkeeper/shopkeeper.module").then((m) => m.ShopkeeperPageModule),
    canActivate: [AuthGuard],
  },
  {
    path: "",
    redirectTo: "identity",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
