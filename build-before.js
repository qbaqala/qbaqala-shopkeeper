const fs = require('fs');
let et = require('elementtree');

module.exports = (ctx) => {

    // if (ctx.build && ctx.build.configuration && ctx.build.configuration === "production") {

        // console.log(ctx,"====ctx====");
        // console.log("production build: performing version bump...");

        // update package.json:
        // let packageJSON = JSON.parse(fs.readFileSync('package.json', 'utf-8').toString());
        // let versionArray = packageJSON.version.split(".");
        // versionArray[0] = (parseInt(versionArray[0])+1).toString();
        // packageJSON.version = versionArray.join(".");
        // fs.writeFileSync('package.json', JSON.stringify(packageJSON, null, "\t"), 'utf-8');
        // console.log("package.json app version updated");
        // console.log("versionArray",versionArray);

        // let prodEnvData = fs.readFileSync(`src/environments/environment.prod.ts`, 'utf-8');
        // prodEnvData = prodEnvData.replace(/CURRENT_VERSION: ".*"/, `CURRENT_VERSION: "${packageJSON.version}"`);
        // fs.writeFileSync('src/environments/environment.prod.ts', prodEnvData, 'utf-8');
        // console.log("environments.prod.ts app version updated");

        let year = new Date().getFullYear();
        year = year.toString().substr(-1);
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let houre = new Date().getHours();
        let minute = new Date().getMinutes();
        if(month < 10){
            month = '0'+month;
        }
        if(date < 10){
            date = '0'+date;
        }
        if(houre < 10){
            houre = '0'+houre;
        }
        if(minute < 10){
            minute = '0'+minute;
        }
        let mainV = '';
        mainV = ''+year+month+date+houre+minute+'';
        console.log(mainV,'<<== mainV')
        
        
        let packageJSON = fs.readFileSync('config.xml', 'utf-8').toString();
        // let versionArray = packageJSON.version;
        packageJSON = et.parse(packageJSON);
        let version = packageJSON.getroot().attrib.myVersion ;
        version = (parseInt(version)+1);
        console.log("version",version);
        // myVersion="${version}"

        let configXmlData = fs.readFileSync('config.xml', 'utf-8');
        // for android
        configXmlData = configXmlData.replace(/android-versionCode=".*" myVersion=".*"/, `id="com.qBaqalaShop.io" version="${mainV}" android-versionCode="${version}" myVersion="${version}" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0"`);
        // for ios
        // configXmlData = configXmlData.replace(/android-versionCode=".*" version=".*"/, `id="com.qBaqalaShopkepeer.app" version="${mainV}" android-versionCode="${version}" myVersion="${version}" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0"`);
        fs.writeFileSync('config.xml', configXmlData,'utf-8');
        console.log("config.xml app version updated");

    // };

};